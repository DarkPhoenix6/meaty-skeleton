#!/bin/sh
set -e
#. ./clean.sh
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub
mkdir -p isodir/dev

cp sysroot/boot/darkphoenixos.kernel isodir/boot/darkphoenixos.kernel
#cp sysroot/boot/myos.kernel isodir/boot/myos.kernel
cat > isodir/boot/grub/grub.cfg << EOF

menuentry "darkphoenixos" {
	multiboot /boot/darkphoenixos.kernel
}
EOF
grub-mkrescue -o darkphoenixos.iso isodir
