#!/bin/sh
set -e
. ./clean.sh
. ./iso.sh

#qemu-system-$(./target-triplet-to-arch.sh $HOST) -serial file:./serial.log -cdrom myos.iso -g1234
#qemu-system-$(./target-triplet-to-arch.sh $HOST) -serial file:./serial.log -s -S kernel/myos.kernel
cd $HOME/meaty-skeleton
i686-elf-objcopy --only-keep-debug kernel/darkphoenixos.kernel kernel.sym
i686-elf-objdump -S --disassemble kernel/darkphoenixos.kernel > ./kernel.stuff
i686-elf-objdump -S --disassemble-all kernel/darkphoenixos.kernel > ./kernel_all.stuff
i686-elf-objdump --disassemble kernel/darkphoenixos.kernel > ./kernel_no_src.stuff
i686-elf-objdump --disassemble-all kernel/darkphoenixos.kernel > ./kernel_all_no_src.stuff
qemu-system-$(./target-triplet-to-arch.sh $HOST) -serial file:./serial.log -s -S -cdrom darkphoenixos.iso -d guest_errors