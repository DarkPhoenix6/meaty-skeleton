#ifndef _STDLIB_H
#define _STDLIB_H 1

#include <sys/cdefs.h>
#include <stddef.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif

#pragma mark - definitions & types -
#define RAND_MAX 2147483647  /* (0x7fffffff) */
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
/* A null pointer constant.  */
// taken from stddef.h in gcc 7.3.0
#if defined (_STDLIB_H) || defined (__need_NULL)
#undef NULL		/* in case <stddef.h> has defined it. */
#ifdef __GNUG__
#define NULL __null
#else   /* G++ */
#ifndef __cplusplus
#define NULL ((void *)0)
#else   /* C++ */
#define NULL 0
#endif  /* C++ */
#endif  /* G++ */
#endif	/* NULL not defined and <stdlib.h> or need NULL.  */
#undef	__need_NULL

#ifndef size_t
#ifndef __size_t
#define __size_t

#endif 
#ifndef __SIZE_TYPE__
#define __SIZE_TYPE__ long unsigned int

#endif
#if !(defined (__GNUG__) && defined (size_t))
typedef __SIZE_TYPE__ size_t;
#endif
#endif

typedef struct {
  int quot;
  int rem;
} div_t;

typedef struct {
  long int quot;
  long int rem;
} ldiv_t;

typedef struct {
  long long quot;
  long long rem;
} lldiv_t;

#if !defined (SYSLIB_DIV) && defined(__is_libk)
#define SYSLIB_DIV 1
#endif

#pragma mark - math stuff -

int abs (int n);

long int labs (long int n);
long long int llabs (long long int n);


div_t div (int numer, int denom);
ldiv_t ldiv (long int numer, long int denom);
lldiv_t lldiv (long long int numer, long long int denom);


#pragma mark - random number generation -
int rand (void);
void srand (unsigned int seed);

__attribute__((__noreturn__))
void abort(void);

void itoa(int, char*, int);
void ltoa(long long, char*, int);
void utoa(unsigned int, char*, int);
void ultoa(unsigned long long, char*, int);
char* btoa(bool);
//#if !defined(__is_libk)
void* malloc (size_t size);
void free (void* ptr);
//#else
//#include <kernel/kheap.h>
//#endif
void* calloc (size_t num, size_t size);
void* realloc (void* ptr, size_t size);




double atof (const char* str);
int atoi (const char * str);
long int atol ( const char * str );
long long int atoll ( const char * str );
int at_quick_exit (void (*func)(void));
double strtod (const char* str, char** endptr);
float strtof (const char* str, char** endptr);
long int strtol (const char* str, char** endptr, int base);
long double strtold (const char* str, char** endptr);
long long int strtoll (const char* str, char** endptr, int base);
unsigned long int strtoul (const char* str, char** endptr, int base);
unsigned long long int strtoull (const char* str, char** endptr, int base);

int atexit (void (*func)(void));
void exit (int status);
char* getenv (const char* name);
_Noreturn void quick_exit (int status);
int system (const char* command);
void _Exit (int status);
#pragma mark - sorting -
void* bsearch (const void* key, const void* base,
               size_t num, size_t size,
               int (*compar)(const void*,const void*));
void qsort (void* base, size_t num, size_t size, int (*compar)(const void*,const void*));
       

int mblen (const char* pmb, size_t max);
int mbtowc (wchar_t* pwc, const char* pmb, size_t max);
int wctomb (char* pmb, wchar_t wc);
size_t mbstowcs (wchar_t* dest, const char* src, size_t max);
size_t wcstombs (char* dest, const wchar_t* src, size_t max);

#ifdef __cplusplus
}
#endif

#endif
