#ifndef _STDIO_H
#define _STDIO_H 1

#include <sys/cdefs.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#define EOF (-1)
#define SEEK_SET 0
typedef struct { int unused; } FILE;
/* A null pointer constant.  */
// taken from stddef.h in gcc 7.3.0
#if defined (_STDIO_H) || defined (__need_NULL)
#undef NULL		/* in case <stddef.h> has defined it. */
#ifdef __GNUG__
#define NULL __null
#else   /* G++ */
#ifndef __cplusplus
#define NULL ((void *)0)
#else   /* C++ */
#define NULL 0
#endif  /* C++ */
#endif  /* G++ */
#endif	/* NULL not defined and <stdio.h> or need NULL.  */
#undef	__need_NULL

#ifndef size_t
#ifndef __size_t
#define __size_t

#endif 
#ifndef __SIZE_TYPE__
#define __SIZE_TYPE__ long unsigned int

#endif
#if !(defined (__GNUG__) && defined (size_t))
typedef __SIZE_TYPE__ size_t;
#endif
#endif


#ifdef __cplusplus
extern "C" {
#endif
extern FILE* stderr;
#define stderr stderr
extern FILE* stdin;
#define stdin stdin
extern FILE* stdout;
#define stdout stdout
int fclose(FILE*);
int fflush(FILE*);
FILE* fopen(const char*, const char*);
int fprintf(FILE*, const char*, ...);
size_t fread(void*, size_t, size_t, FILE*);
int fseek(FILE*, long, int);
long ftell(FILE*);
size_t fwrite(const void*, size_t, size_t, FILE*);
void setbuf(FILE*, char*);
int vfprintf(FILE*, const char*, va_list);
int printf(const char* __restrict, ...);
void unsigned_to_string_submethod(volatile char*, unsigned long long, int);
int printf_c(char);
int printf_b(bool);
int print_u_int8_t(uint8_t);
int print_u_int16_t(uint16_t);
int print_u_short(unsigned short);
int print_u_int(unsigned int);
int print_u_long_long(unsigned long long);
int putchar(int);
int puts(const char*);
int getchar ( void ); // It is equivalent to calling getc with stdin as argument.
char * gets ( char * str );  /* Reads characters from the standard input (stdin) and stores them as a C string into str until a newline character or the end-of-file is reached.

The newline character, if found, is not copied into str.
*/

int sprintf ( char * str, const char * format, ... );
int snprintf ( char * s, size_t n, const char * format, ... );

#ifdef __cplusplus
}
#endif

#endif
