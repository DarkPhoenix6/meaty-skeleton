#ifndef _UNISTD_H
#define _UNISTD_H
#include <sys/types.h>
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif
//extern char **environ;
#define weak_alias(name, aliasname) \
  extern __typeof (name) aliasname __attribute__ ((__weak__, __alias__ (#name)));

#if 0
#define weak_extern(symbol) _weak_extern (symbol)
#define _weak_extern(symbol) asm (".weak " #symbol);
#endif

#define weak_function __attribute__ ((__weak__))
void	_exit (int __status) __attribute__ ((__noreturn__));
int execv(const char*, char* const[]);
int execve(const char*, char* const[], char* const[]);
int execvp(const char*, char* const[]);
pid_t fork(void);
extern int __brk (void *__addr);
void *__sbrk (intptr_t __delta);


#define STDIN_FILENO    0       /* standard input file descriptor */
#define STDOUT_FILENO   1       /* standard output file descriptor */
#define STDERR_FILENO   2       /* standard error file descriptor */
#ifdef __cplusplus
}
#endif
#endif
