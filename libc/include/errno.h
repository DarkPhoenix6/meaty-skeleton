#ifndef	_ERRNO_H
#define	_ERRNO_H 1
#ifdef __cplusplus
extern "C" {
#endif
#ifndef	errno
extern int errno;
#endif

# define __set_errno(val) (errno = (val))




#if defined __USE_GNU || defined __need_error_t
# ifndef __error_t_defined
typedef int error_t;
#  define __error_t_defined	1
# endif
# undef __need_error_t
#endif
#ifdef __cplusplus
}
#endif
#include <sys/errno.h>

#endif /* errno.h */
