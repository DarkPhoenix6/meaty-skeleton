#ifndef _CTYPE_H
#define _CTYPE_H 1

int tolower ( int );
int toupper ( int );
int islower ( int );
int isupper ( int );

int isalnum ( int c );
int isalpha ( int c );
int isblank ( int c ); // '\t' or ' '
int iscntrl ( int c ); // For the standard ASCII character set (used by the "C" locale), control characters are those between ASCII codes 0x00 (NUL) and 0x1f (US), plus 0x7f (DEL).

int isdigit ( int c );

int isgraph ( int c ); // isgraph returns true for the same cases as isprint except for the space character (' '), which returns true when checked with isprint but false when checked with isgraph.

int isprint ( int c ); // For the standard ASCII character set (used by the "C" locale), printing characters are all with an ASCII code greater than 0x1f (US), except 0x7f (DEL).

int ispunct ( int c ); // The standard "C" locale considers punctuation characters all graphic characters (as in isgraph) that are not alphanumeric (as in isalnum).

int isspace ( int c ); // ' ', '\t', '\n', 
/*For the "C" locale, white-space characters are any of:
' '	(0x20)	space (SPC)
'\t'	(0x09)	horizontal tab (TAB)
'\n'	(0x0a)	newline (LF)
'\v'	(0x0b)	vertical tab (VT)
'\f'	(0x0c)	feed (FF)
'\r'	(0x0d)	carriage return (CR)
*/

int isxdigit( int c ); // Hexadecimal digits are any of: 0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F
#endif /* _CTYPE_H */