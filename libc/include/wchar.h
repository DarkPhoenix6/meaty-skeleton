#ifndef _WCHAR_H
#define _WCHAR_H 1

#ifdef __cplusplus
extern "C" {
#endif

/* A null pointer constant.  */
// taken from stddef.h in gcc 7.3.0
#if defined (_WCHAR_H) || defined (__need_size_t)
#undef size_t
#ifndef size_t
#ifndef __size_t
#define __size_t

#endif 
#ifndef __SIZE_TYPE__
#define __SIZE_TYPE__ long unsigned int

#endif
#if !(defined (__GNUG__) && defined (size_t))
typedef __SIZE_TYPE__ size_t;
#endif
#endif


wchar_t* wmemset (wchar_t* ptr, wchar_t wc, size_t num);
wchar_t* wmemcpy (wchar_t* destination, const wchar_t* source, size_t num);
wchar_t* wmemmove (wchar_t* destination, const wchar_t* source, size_t num);
wchar_t* wcsncpy (wchar_t* destination, const wchar_t* source, size_t num);


#ifdef __cplusplus
}
#endif
#endif /* _WCHAR_H */
