#ifndef _STRING_H
#define _STRING_H 1

#include <sys/cdefs.h>

#include <stddef.h>
#ifndef size_t
#ifndef __size_t
#define __size_t

#endif 
#ifndef __SIZE_TYPE__
#define __SIZE_TYPE__ long unsigned int

#endif
#if !(defined (__GNUG__) && defined (size_t))
typedef __SIZE_TYPE__ size_t;
#endif
#endif
#ifdef __cplusplus
extern "C" {
#endif

int memcmp(const void*, const void*, size_t);
void* memcpy(void* __restrict, const void* __restrict, size_t);
void* memmove(void*, const void*, size_t);
void* memset(void*, int, size_t);
size_t strlen(const char*);
void strreverse(char*, char*);
int strcmp(const char *, const char *);
char * strcat ( char * , const char * );

#ifdef __cplusplus
}
#endif

#endif
