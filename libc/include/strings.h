#ifndef _STRINGS_H
#define _STRINGS_H 1

#include <stddef.h>
#ifdef __cplusplus
extern "C" {
#endif
int    bcmp(const void *, const void *, size_t);
void   bcopy(const void *, void *, size_t);
void   bzero(void *, size_t);
/*
The ffs(),	ffsl() and ffsll() functions find the first bit	set (beginning
     with the least significant	bit) in	value and return the index of that
     bit.

     The fls(),	flsl() and flsll() functions find the last bit set in value
     and return	the index of that bit.

     Bits are numbered starting	at 1 (the least	significant bit).  A return
     value of zero from	any of these functions means that the argument was
     zero.

*/

int    ffs(int value);
int   ffsl(long value);
int  ffsll(long	long value);
int     fls(int value);
int     flsl(long value);
int     flsll(long	long value);
char   *index(const char *, int);
char   *rindex(const char *, int);
int    strcasecmp(const char *, const char *);
int    strncasecmp(const char *, const char *, size_t);
#ifdef __cplusplus
}
#endif

#endif /* _STRINGS_H */
