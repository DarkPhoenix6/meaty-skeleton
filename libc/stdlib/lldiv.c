#include <stdlib.h>


lldiv_t lldiv (long long int numer, long long int denom)
{
    lldiv_t res;
    res.quot = numer / denom;
    res.rem = numer % denom;
    return res;
}
