#include <stdlib.h>
#define DEBUG_QSORT 1
#undef DEBUG_QSORT
#ifdef DEBUG_QSORT
#include <kernel/com_one.h>
#include <stdio.h>
#endif /* DEBUG_QSORT */
#undef INTROSORT
#undef INTROSORT_MATH
#ifdef INTROSORT_MATH
#include <math.h>
#else
#include <strings.h>
#endif /* INTROSORT_MATH */
#define min(a, b) (a) < (b) ? a : b
typedef int (*cmp_t) (const void*, const void*);
#define CMP(x, y) ((*cmp)((const void *)(x), (const void *)(y)))
//#ifdef __LP64__

static inline char* med3(char*, char*, char*, cmp_t) __attribute__((always_inline));
#ifdef INTROSORT_MATH
#define DEPTH(x) log10(pow((double)x, (double)2) )
#else
#define DEPTH(x) (2 * (flsll((long long)(x)) - 1))
#endif /* INTROSORT_MATH */
/* Byte-wise swap two items of size SIZE. */
#define SWAP(a, b, size)                                                      \
  do                                                                              \
    {                                                                              \
      size_t __size = (size);                                                      \
      char *__a = (a), *__b = (b);                                              \
      do                                                                      \
        {                                                                      \
          char __tmp = *__a;                                                      \
          *__a++ = *__b;                                                      \
          *__b++ = __tmp;                                                      \
        } while (--__size > 0);                                                      \
    } while (0)
//static inline void swap()
//{}

static inline char* med3(char* a, char* b, char* c, cmp_t cmp)
{
    return CMP(a, b) < 0 ? (CMP(b, c) < 0 ? b : (CMP(a, c) < 0 ? c : a)) : (CMP(b, c) > 0 ? b : (CMP(a, c) < 0 ? a : c));
}
#ifdef INTROSORT
static void _qsort(void* base, char*lo, char*hi, size_t num, size_t size, cmp_t* cmp, int depth_limit)
#else
static void _qsort(void* base, char*lo, char*hi, size_t num, size_t size, cmp_t cmp)
#endif
{
    #ifdef DEBUG_QSORT
    int print_debug = 1;
    write_serial_string("_qsort: %i, &lo[0]: 0x%X, &hi[0]:0x%X\n", print_debug++, &lo[0], &hi[0]); // 1
    #endif
    //char *pa, *pb, *pc, *pd, *pl, *pm, *pn;
    char* pivot;
	  //size_t d, r;
    char *mid = &lo[size * (int)(num / 2)];

    
    if (&lo[0] >= &hi[0])
    {
      return;
    }
    else
    {
    #ifdef INTROSORT
    depth_limit--;
    if ( depth_limit < 0)
    {
      // heapsort this section of array
      return;
    }
    #endif /* INTROSORT */
      // *pivot = med3(lo, mid, hi, cmp);
      // median of 3
      if ( (CMP(hi,lo) < 0 ? 1 : 0))
      {
        SWAP(lo, hi, size);
      }
     if ((CMP(mid, lo) < 0 ? 1 : 0))
       { 
         SWAP(lo, mid, size);
       }
     if ((CMP(hi, mid) < 0 ? 1 : 0))
       { 
         SWAP(hi, mid, size);
       }
      SWAP(lo, mid, size) ;
      pivot = &lo[0];
      unsigned long pivot_pos=0;
      //partition
      unsigned long i;
      #ifdef DEBUG_QSORT
    
    write_serial_string("_qsort: %i, &lo[0]: 0x%X, &hi[0]:0x%X, pivot: 0x%X\n", print_debug++, &lo[0], &hi[0], pivot); // 2
    #endif
      for (i = 0; i < num; i++)
      {
        if ((CMP(&lo[size * i], pivot) < 0 ? 1 : 0))
        {
          SWAP(&lo[i * size], &pivot[1 * size], size);
          SWAP(pivot, &pivot[1 * size], size);
          pivot = &pivot[1 * size];
          pivot_pos++;
        }
      }
      
      //_qsort Left side
    #ifdef INTROSORT
      _qsort(base, lo, &lo[size * (pivot_pos - 1)], (size_t)pivot_pos, size, cmp, (int) depth_limit);
    #else
    _qsort(base, lo, &lo[size * (pivot_pos - 1)], (size_t)pivot_pos, size, cmp);
    #endif /* INTROSORT */
      //_qsort Right side
    #ifdef INTROSORT
      _qsort(base, &pivot[1 * size], hi, num - (size_t)pivot_pos, size, cmp, (int) depth_limit);
    #else
    _qsort(base, &pivot[1 * size], hi, num - (size_t)pivot_pos, size, cmp);
    #endif /* INTROSORT */
    }
       
}
void qsort (void* base, size_t num, size_t size, cmp_t cmp)
{
    void * last = base + ((num-1) * size);
    char *lo = (char*) base;
    
    char *hi = &lo[size * (num - 1)];
    #ifdef INTROSORT
    #ifdef __LP64__
    
    #endif /* __LP64__ */
    #ifdef INTROSORT_MATH
    double dep = DEPTH(num) ;
    #else
    int dep = DEPTH(num) ;
    #endif /* INTROSORT_MATH */
    _qsort(base, lo, hi, num, size, cmp, (int) dep);
    #else
    _qsort(base, lo, hi, num, size, cmp);
    #endif /* INTROSORT */
    return;
}
