#include <ctype.h>
#include <stddef.h>
#include <stdint.h>

int isprint ( int c )
{
  if ( ( c >= 0x20 && c <= 0x7e ) )
    return 1;
  else
    return 0;
}
