#include <ctype.h>
#include <stddef.h>
#include <stdint.h>

int isdigit ( int c )
{
  if ( c >= '0' && c <= '9' )
    return 1;
  else
    return 0;
}
