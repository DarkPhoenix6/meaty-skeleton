#include <ctype.h>
#include <stddef.h>
#include <stdint.h>

int isgraph ( int c )
{
  if ( c >= 0x21 && c <= 0x7e )
    return 1;
  else
    return 0;
}
