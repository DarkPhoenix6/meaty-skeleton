#include <ctype.h>
#include <stddef.h>
#include <stdint.h>

int isxdigit ( int c )
{
  if((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f') || ( c >= '0' && c <= '9' ) ) 
  {
    return 1;
  }
  else
    return 0;
}
