#include <stdio.h>

#if defined(__is_libk)
#include <kernel/tty.h>
#else
#include <kernel/syscall.h>
#endif

int putchar(int ic) {
#if defined(__is_libk)
	char c = (char) ic;
	terminal_write(&c, sizeof(c));
#else
	// TODO: Implement stdio and the write system call.
   char c = (char) ic;
   syscall_terminal_write(&c, sizeof(c));
#endif
	return ic;
}
