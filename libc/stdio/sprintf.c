#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>


int sprintf ( char * str, const char * format, ... )

{
char * str_start = str;
va_list parameters;
	va_start(parameters, format);

	int written = 0;

	while (*format != '\0') {
		size_t maxrem = INT_MAX - written;

		if (format[0] != '%' || format[1] == '%') {
			if (format[0] == '%')
				format++;
			size_t amount = 1;
			while (format[amount] && format[amount] != '%')
				amount++;
			if (maxrem < amount) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
	    
      memcpy(str_start, format, amount);
      
      str_start+= amount;
			format += amount;
			written += amount;
			continue;
		}

		const char* format_begun_at = format++;

		if (*format == 'c') {
			format++;
			char c = (char) va_arg(parameters, int /* char promotes to int */);
			if (!maxrem) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
      str_start[0] = c;
			str_start++;
			written++;
		} else if (*format == 's') {
			format++;
			const char* str = va_arg(parameters, const char*);
			size_t len = strlen(str);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
     
			memcpy(str_start, str, len);
      str_start += len;
      /* str_start += len - 1;
      if (str_start[0] != '\0')
        str_start++;
        */
			written += len;
		} else if (*format == 'i' || *format == 'd') {
       format++;
       //char* str2 = (char*) 15;
       int num = (int) va_arg(parameters, int);
       //itoa(num, str2, 10);
       char * str = malloc(67);
       itoa(num, str, 10);
       //const char* str3 = convert(num,10);
       /*if (num == 55)
         puts("true");*/
       //size_t w = position_of_null(str3);
       size_t w = strlen(str);
       
       if (w < 0)
       {
         // TODO: Set errno to Error.
         free(str);
				  return -1;
       } else if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
         free(str);
				  return -1;
			 } else
        {
          /*if (!print(str3, w))
				    return -1;*/
          memcpy(str_start, str, w);
          written += w;
          str_start += w;
          //str_start += w - 1;
          //if (str_start[0] != '\0')
            //str_start++;
          free(str);
        }
        
      } else if (*format == 'o') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       char * str = malloc(67);
       utoa(num, str, 8);
       //const char* str3 = uconvert(num,8);
       //size_t w = position_of_null(str3);
       size_t len = strlen(str);
        if ( maxrem < len) 
       {
         // TODO: Set errno to EOVERFLOW.
         free(str);
				  return -1;
			 } else
        {
          //if (!print(str3, w))
				    //return -1;
          memcpy(str_start, str, len);
          free(str);
          str_start += len;          
          written += len;
        }
    } else if (*format == 'u') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       //const char* str3 = uconvert(num,10);
       //size_t w = position_of_null(str3);
       //size_t w = printf(str3);
       char * str = malloc(67);
       utoa(num, str, 10);
       size_t len = strlen(str);
        if ( maxrem < len) 
       {
         // TODO: Set errno to EOVERFLOW.
         free(str);
				  return -1;
			 } else
        {
          
          //if (!print(str3, w))
				    //return -1;
          memcpy(str_start, str, len);
          free(str);
          str_start += len;    
          written += len;
        }
    } else if (*format == 'x') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       //const char* str3 = uconvert(num,16);
       //size_t w = position_of_null(str3);
        char * str = malloc(67);
       utoa(num, str, 16);
       size_t len = strlen(str);
        if ( maxrem < len) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          //if (!print(str3, w))
				    //return -1;
          memcpy(str_start, str, len);
          free(str);
          str_start += len;    
          written += len;
        }
    } else if (*format == 'X') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       // TODO: Make const
       //char* str3 = uconvert(num,16);
       char * str = malloc(67);
       utoa(num, str, 16);
       size_t len = strlen(str);
       int i = 0;
       while (str[i])
       {
           str[i] = (char) toupper(str[i]);
           i++;
       }
       //size_t w = position_of_null(str3);
       //const char* str4 = str3;
        if ( maxrem < len) 
       {
         // TODO: Set errno to EOVERFLOW.
         free(str);
				  return -1;
			 } else
        {
          //if (!print(str4, w))
				    //return -1;
          memcpy(str_start, str, len);
          free(str);
          str_start += len;
          written += len;
        }
    } else {
			format = format_begun_at;
			size_t len = strlen(format);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			//if (!print(format, len))
				//return -1;
      memcpy(str_start, format, len);
			written += len;
      str_start += len;
			format += len;
		}
	}

	va_end(parameters);
  str_start[0] = '\0'; 
	return written;

}