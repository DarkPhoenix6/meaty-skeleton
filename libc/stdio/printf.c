#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>

char* convert(int num, int base)
{
  //memory leak ?
  //const char* str = (char*) 67;
  char str[67];
  char* str_ptr = str;
  itoa(num, str_ptr, base);
  return str_ptr;
}

char* uconvert(unsigned int num, int base)
{
  //memory leak ?
  //const char* str = (char*) 67;
  //const char* str[67];
  char str[67];
  char* str_ptr = str;
  utoa(num, str_ptr, base);
  return str_ptr;
}

int position_of_null(const char* str)
{
    size_t len = strlen(str);
    int l = 0;
    for (size_t i = 0; i < len; i++)
    {
        if (str[i] == '\0')
          break;
        l++;
    }
    return l;
}

static bool print(const char* data, size_t length) {
	const unsigned char* bytes = (const unsigned char*) data;
	for (size_t i = 0; i < length; i++)
		if (putchar(bytes[i]) == EOF)
			return false;
	return true;
}

int printf(const char* restrict format, ...) {
	va_list parameters;
	va_start(parameters, format);

	int written = 0;

	while (*format != '\0') {
		size_t maxrem = INT_MAX - written;

		if (format[0] != '%' || format[1] == '%') {
			if (format[0] == '%')
				format++;
			size_t amount = 1;
			while (format[amount] && format[amount] != '%')
				amount++;
			if (maxrem < amount) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(format, amount))
				return -1;
			format += amount;
			written += amount;
			continue;
		}

		const char* format_begun_at = format++;

		if (*format == 'c') {
			format++;
			char c = (char) va_arg(parameters, int /* char promotes to int */);
			if (!maxrem) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(&c, sizeof(c)))
				return -1;
			written++;
		} else if (*format == 's') {
			format++;
			const char* str = va_arg(parameters, const char*);
			size_t len = strlen(str);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
     
			if (!print(str, len))
				return -1;
			written += len;
		} else if (*format == 'i' || *format == 'd') {
       format++;
       //char* str2 = (char*) 15;
       int num = (int) va_arg(parameters, int);
       //itoa(num, str2, 10);
       const char* str3 = convert(num,10);
       /*if (num == 55)
         puts("true");*/
       //size_t w = position_of_null(str3);
       size_t w = printf("%s",str3);
       if (w < 0)
       {
         // TODO: Set errno to Error.
				  return -1;
       } else if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          /*if (!print(str3, w))
				    return -1;*/
          written += w;
        }
        
      } else if (*format == 'o') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       const char* str3 = uconvert(num,8);
       size_t w = position_of_null(str3);
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          if (!print(str3, w))
				    return -1;
          written += w;
        }
    } else if (*format == 'u') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       const char* str3 = uconvert(num,10);
       size_t w = position_of_null(str3);
       //size_t w = printf(str3);
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          
          if (!print(str3, w))
				    return -1;
          written += w;
        }
    } else if (*format == 'x') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       const char* str3 = uconvert(num,16);
       size_t w = position_of_null(str3);
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          if (!print(str3, w))
				    return -1;
          written += w;
        }
    } else if (*format == 'X') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       // TODO: Make const
       char* str3 = uconvert(num,16);
       
       int i = 0;
       while (str3[i])
       {
           str3[i] = (char) toupper(str3[i]);
           i++;
       }
       size_t w = position_of_null(str3);
       const char* str4 = str3;
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          if (!print(str4, w))
				    return -1;
          written += w;
        }
    } else {
			format = format_begun_at;
			size_t len = strlen(format);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!print(format, len))
				return -1;
			written += len;
			format += len;
		}
	}

	va_end(parameters);
	return written;
}

void unsigned_to_string_submethod(volatile char* number, unsigned long long val, int index)
{
    uint8_t remainder = 0;
  while (--index > -1)
    {
        remainder = val % 10;
        val = val / 10;
        if (remainder == 0)
          number[index] = '0';
        else if (remainder == 1)
          number[index] = '1';
        else if (remainder == 2)
          number[index] = '2';
        else if (remainder == 3)
          number[index] = '3';
        else if (remainder == 4)
          number[index] = '4';
        else if (remainder == 5)
          number[index] = '5';
        else if (remainder == 6)
          number[index] = '6';
        else if (remainder == 7)
          number[index] = '7';
        else if (remainder == 8)
          number[index] = '8';
        else
          number[index] = '9';
    }
}

int printf_c(char stuff)
{
    volatile char* c = (char*) 3;
    c[0] = stuff;
    c[1] = '\n';
    c[2] = '\0';
    return printf(c);
}

int printf_b(bool stuff)
{
  if (stuff == true)
    return printf("true\n");
  else
    return printf("false\n");
}

int print_u_int8_t(uint8_t stuff)
{
    uint8_t val = stuff;
    int array_size = 0;
    int index = 0;
    if (stuff >= 100)
      array_size = 4;
    else if (stuff >= 10)
      array_size = 3;
    else
      array_size = 2;
    
    array_size += 1;
    volatile char* number = (char*) array_size;
    index = array_size - 1;
    number[index] = '\0';
    index -= 1;
    number[index] = '\n';
    
    unsigned_to_string_submethod(number, val, index);
    return printf(number);
}


int print_u_int16_t(uint16_t stuff)
{
    uint16_t max_val = 0xFFFF;
    uint16_t val = stuff;
    int array_size = 0;
    int index = 0;
    if (stuff >= 10000)
      array_size = 6;
    else if (stuff >= 1000)
      array_size = 5;
    else if (stuff >= 100)
      array_size = 4;
    else if (stuff >= 10)
      array_size = 3;
    else
      array_size = 2;
    
    array_size += 1;
    volatile char* number = (char*) array_size;
    index = array_size - 1;
    number[index] = '\0';
    index -= 1;
    number[index] = '\n';
    unsigned_to_string_submethod(number, val, index);
    return printf(number);
}

int print_u_short(unsigned short stuff)
{
    unsigned short val = stuff;
    int array_size = 0;
    int index = 0;
    if (stuff >= 10000)
      array_size = 6;
    else if (stuff >= 1000)
      array_size = 5;
    else if (stuff >= 100)
      array_size = 4;
    else if (stuff >= 10)
      array_size = 3;
    else
      array_size = 2;
    
    array_size += 1;
    volatile char* number = (char*) array_size;
    index = array_size - 1;
    number[index] = '\0';
    index -= 1;
    number[index] = '\n';
    
    unsigned_to_string_submethod(number, val, index);
    return printf(number);
}

int print_u_int(unsigned int stuff)
{
    uint8_t remainder = 0;
    unsigned int val = stuff;
    int array_size = 0;
    int index = 0;
    // 4294967295
    if (stuff >= 1000000000)
      array_size = 11;
    else if (stuff >= 100000000)
      array_size = 10;
    else if (stuff >= 10000000)
      array_size = 9;
    else if (stuff >= 1000000)
      array_size = 8;
    else if (stuff >= 100000)
      array_size = 7;
    else if (stuff >= 10000)
      array_size = 6;
    else if (stuff >= 1000)
      array_size = 5;
    else if (stuff >= 100)
      array_size = 4;
    else if (stuff >= 10)
      array_size = 3;
    else
      array_size = 2;
    
    array_size += 1;
    volatile char* number = (char*) array_size;
    index = array_size - 1;
    number[index] = '\0';
    index -= 1;
    number[index] = '\n';
    
    unsigned_to_string_submethod(number, val, index);
    return printf(number);
}

int print_u_long_long(unsigned long long stuff)
{
    uint8_t remainder = 0;
    unsigned long long val = stuff;
    int array_size = 0;
    int index = 0;
    // 18446744073709551615
    if (stuff >= 10000000000000000000)
      array_size = 21;
    else if (stuff >= 1000000000000000000)
      array_size = 20;
    else if (stuff >= 100000000000000000)
      array_size = 19;
    else if (stuff >= 10000000000000000)
      array_size = 18;
    else if (stuff >= 1000000000000000)
      array_size = 17;
    else if (stuff >= 100000000000000)
      array_size = 16;
    else if (stuff >= 10000000000000)
      array_size = 15;
    else if (stuff >= 1000000000000)
      array_size = 14;
    else if (stuff >= 100000000000)
      array_size = 13;
    else if (stuff >= 10000000000)
      array_size = 12;
    else if (stuff >= 1000000000)
      array_size = 11;
    else if (stuff >= 100000000)
      array_size = 10;
    else if (stuff >= 10000000)
      array_size = 9;
    else if (stuff >= 1000000)
      array_size = 8;
    else if (stuff >= 100000)
      array_size = 7;
    else if (stuff >= 10000)
      array_size = 6;
    else if (stuff >= 1000)
      array_size = 5;
    else if (stuff >= 100)
      array_size = 4;
    else if (stuff >= 10)
      array_size = 3;
    else
      array_size = 2;
    
    array_size += 1;
    volatile char* number = (char*) array_size;
    index = array_size - 1;
    number[index] = '\0';
    index -= 1;
    number[index] = '\n';
    
    unsigned_to_string_submethod(number, val, index);
    return printf(number);
}
