#undef GLIB_STUFF
#ifdef GLIB_STUFF
/* Copyright (C) 1991,1995-1997,2000,2002,2009 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.
   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <errno.h>
#include <stdint.h>
#include <unistd.h>

/* Defined in brk.c.  */
extern void *__curbrk;
extern int __brk (void *addr);

/* Defined in init-first.c.  */
extern int __libc_multiple_libcs attribute_hidden;

/* Extend the process's data space by INCREMENT.
   If INCREMENT is negative, shrink data space by - INCREMENT.
   Return start of new space allocated, or -1 for errors.  */
void *
__sbrk (intptr_t increment)
{
  void *oldbrk;

  /* If this is not part of the dynamic library or the library is used
     via dynamic loading in a statically linked program update
     __curbrk from the kernel's brk value.  That way two separate
     instances of __brk and __sbrk can share the heap, returning
     interleaved pieces of it.  */
  if (__curbrk == NULL || __libc_multiple_libcs)
    if (__brk (0) < 0)		/* Initialize the break.  */
      return (void *) -1;

  if (increment == 0)
    return __curbrk;

  oldbrk = __curbrk;
  if ((increment > 0
       ? ((uintptr_t) oldbrk + (uintptr_t) increment < (uintptr_t) oldbrk)
       : ((uintptr_t) oldbrk < (uintptr_t) -increment))
      || __brk (oldbrk + increment) < 0)
    return (void *) -1;

  return oldbrk;
}
libc_hidden_def (__sbrk)
weak_alias (__sbrk, sbrk)

#else 
#include <unistd.h>
#ifdef ONE_SBREAK
extern end;

static void *curbrk = (char*)&end;

void *
sbrk(incr)
int incr; {
	extern int errno;
	extern int _brk(void *);
	void *ptr = curbrk;
	int t;

	if (incr == 0)
		return curbrk;
	t = _brk (curbrk + incr);
	if (t == -1 && errno)
		return (void *)-1;
	curbrk = ((char *)curbrk) + incr;
	return ptr;
}
#else



#define SBRK_ONE 1
#ifdef SBRK_ONE
/* Defined in brk.c.  */
extern void *__curbrk;
extern int __brk (void *addr);
#include <stdio.h>
#include <stdint.h>
void* sbrk(intptr_t incr) {
  extern char _end;		/* Defined by the linker */
  char *prev_heap_end;
  register char * stack_ptr asm("esp");
   printf("0x%X\n", __curbrk);
  if (__curbrk == NULL) {
    __curbrk = &_end;
  }
  prev_heap_end = __curbrk;
  if (__curbrk + incr > stack_ptr) {
    printf("__curbrk: 0x%X\nincr: 0x%X\n__curbrk + incr: 0x%X\nstack_ptr: 0x%X\n", __curbrk, incr,__curbrk + incr, stack_ptr);
    printf("Heap and stack collision\n");
    //write (1, "Heap and stack collision\n", 25);
    abort ();
  }

  __curbrk += incr;
  return (void*) prev_heap_end;
}
#else
void* sbrk(int incr) {
  extern char _end;		/* Defined by the linker */
  static char *heap_end;
  char *prev_heap_end;
   char * stack_ptr asm("esp");
  if (heap_end == 0) {
    heap_end = &_end;
  }
  prev_heap_end = heap_end;
  if (heap_end + incr > stack_ptr) {
    write (1, "Heap and stack collision\n", 25);
    abort ();
  }

  heap_end += incr;
  return (void*) prev_heap_end;
}
#endif
#endif

#endif

