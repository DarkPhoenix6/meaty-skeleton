/* Copyright (C) 1991, 1995, 1996, 1997, 2002 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.
   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <errno.h>
#include <unistd.h>
#include <stddef.h> /* for NULL */
#define weak_alias(name, aliasname) \
  extern __typeof (name) aliasname __attribute__ ((__weak__, __alias__ (#name)));

#if 0
#define weak_extern(symbol) _weak_extern (symbol)
#define _weak_extern(symbol) asm (".weak " #symbol);
#endif

#define weak_function __attribute__ ((__weak__))
extern end;


/* sbrk.c expects this.  */
void *__curbrk = NULL;

/* Set the end of the process's data space to ADDR.
   Return 0 if successful, -1 if not.  */
int __brk (addr)
     void *addr;
{
  void *__curbrk = addr;
  // __set_errno (ENOSYS);
  __set_errno (-1);
  return -1;
}
stub_warning (brk);

weak_alias (__brk, brk)