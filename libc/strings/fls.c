#include <strings.h>

int fls(int value)
{
    unsigned int testVal;
    int bit_loc = sizeof(int)*8;
    for (testVal = (0x1 << ((sizeof(int)*8)-1)); testVal > 0x0; testVal = (testVal >> 1))
    {
      if (testVal & value)
      {
        return bit_loc;
      }
      else 
      {
        bit_loc--;
      }
    }
    return 0;
}
