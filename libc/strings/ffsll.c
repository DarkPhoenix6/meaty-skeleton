#include <strings.h>

int ffsll(long long value)
{
    unsigned long long testVal;
    int bit_loc = 0;
    for (testVal = 0x1; bit_loc < (sizeof(long long)*8); testVal = (testVal << 1))
    {
      bit_loc++;
      if (testVal & value)
      {
        return bit_loc;
      }
    }
    return 0;
}
