#include <strings.h>

int flsl(long value)
{
    unsigned long testVal;
    int bit_loc = sizeof(long)*8;
    for (testVal = (0x1 << ((sizeof(long)*8)-1)); testVal > 0x0; testVal = (testVal >> 1))
    {
      if (testVal & value)
      {
        return bit_loc;
      }
      else 
      {
        bit_loc--;
      }
    }
    return 0;
}
