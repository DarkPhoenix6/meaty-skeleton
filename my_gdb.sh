#!/bin/bash

# exit on error
set -e
CURRDIR=$(pwd)
i686-elf-objcopy --only-keep-debug kernel/darkphoenixos.kernel kernel.sym
i686-elf-objdump -S --disassemble kernel/darkphoenixos.kernel > kernel.stuff
gdb -ex "symbol-file ${CURRDIR}/kernel.sym" \
-ex 'set architecture i386' \
-ex 'target remote localhost:1234' \
-ex 'break _start' \
-ex 'break kernel_main' \
-ex 'b move_stack' \
-ex 'b initialise_tasking' \
-ex 'b switch_to_user_mode' \
-ex 'b hop_to_user_mode' \
-ex 'b *&syscall_handler+73' \
-ex 'b syscall_handler' \
-ex 'continue'


exit
# :)
