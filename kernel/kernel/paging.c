// paging.c -- Defines the interface for and structures relating to paging.
//             Written for JamesM's kernel development tutorials.
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <kernel/system.h>
#include <kernel/paging.h>
#include <kernel/kheap.h>
#include <kernel/com_one.h>
#include <irqs/inline_assembly.h>
#define DEBUG_PAGING_C 1
#define PAGING_PERMISSIONS 1
// The kernel's page directory
page_directory_t *kernel_directory=0;

// The current page directory;
page_directory_t *current_directory=0;

// A bitset of frames - used or free.
uint32_t *frames;
uint32_t nframes;

// Defined in kheap.c
extern uint32_t placement_address;
extern heap_t *kheap;

// defined in pagingAssembly.s
extern void loadPageDirectory(unsigned int*);
extern void enablePaging();

// Macros used in the bitset algorithms.
#define INDEX_FROM_BIT(a) (a/(8*4))
#define OFFSET_FROM_BIT(a) (a%(8*4))

// Static function to set a bit in the frames bitset
static void set_frame(uint32_t frame_addr)
{
    uint32_t frame = frame_addr/0x1000;
    uint32_t idx = INDEX_FROM_BIT(frame);
    uint32_t off = OFFSET_FROM_BIT(frame);
    frames[idx] |= (0x1 << off);
}

// Static function to clear a bit in the frames bitset
static void clear_frame(uint32_t frame_addr)
{
    uint32_t frame = frame_addr/0x1000;
    uint32_t idx = INDEX_FROM_BIT(frame);
    uint32_t off = OFFSET_FROM_BIT(frame);
    frames[idx] &= ~(0x1 << off);
}

// Static function to test if a bit is set.
static uint32_t test_frame(uint32_t frame_addr)
{
    uint32_t frame = frame_addr/0x1000;
    uint32_t idx = INDEX_FROM_BIT(frame);
    uint32_t off = OFFSET_FROM_BIT(frame);
    return (frames[idx] & (0x1 << off));
}

// Static function to find the first free frame.
static uint32_t first_frame()
{
    uint32_t i, j;
    for (i = 0; i < INDEX_FROM_BIT(nframes); i++)
    {
        if (frames[i] != 0xFFFFFFFF) // nothing free, exit early.
        {
            // at least one bit is free here.
            for (j = 0; j < 32; j++)
            {
                uint32_t toTest = 0x1 << j;
                if ( !(frames[i]&toTest) )
                {
                    return i*4*8+j;
                }
            }
        }
    }
    return (uint32_t)-1;//0xFFFFFFFF; // TODO: improve error handling
}

// Function to allocate a frame.
void alloc_frame(page_t *page, int is_kernel, int is_writeable)
{
    if (page->frame != 0)
    {
        return;
    }
    else
    {
        uint32_t idx = first_frame();
        if (idx == (uint32_t)-1)
        {
            // PANIC! no free frames!!
            PANIC("No free Frames!");
        }
        set_frame(idx*0x1000);
        page->present = 1;
        page->rw = (is_writeable)?1:0;
        page->user = (is_kernel)?0:1;
        page->frame = idx;
    }
}

// Function to deallocate a frame.
void free_frame(page_t *page)
{
    uint32_t frame;
    if (!(frame=page->frame))
    {
        return;
    }
    else
    {
        clear_frame(frame);
        page->frame = 0x0;
    }
}

void initialise_paging()
{
    // The size of physical memory. For the moment we 
    // assume it is 16MB big.
    uint32_t mem_end_page = 0x1000000;
    extern char _stext;
    extern char _etext;
    extern char _srodata;
    extern char _erodata;
    extern char  _sdata;
    extern char _edata;
    extern char _sbss;
    extern char _ebss;
    extern char _kernel_start;
    extern char endkernel;
    extern char _kernel_end;
    nframes = mem_end_page / 0x1000;
    frames = (uint32_t*)kmalloc(INDEX_FROM_BIT(nframes));
    memset(frames, 0, INDEX_FROM_BIT(nframes));
    #ifdef DEBUG_PAGING_C
    int print_debug = 1;
    write_serial_string("initialise_paging: %i\n", print_debug++); // 1
    #endif
    //write_serial_string("hi init_page 1\n");
    
    // Let's make a page directory.
    uint32_t phys;
    kernel_directory = (page_directory_t*)kmalloc_a(sizeof(page_directory_t));
    memset(kernel_directory, 0, sizeof(page_directory_t));
    current_directory = kernel_directory;
    kernel_directory->physicalAddr = (uint32_t)kernel_directory->tablesPhysical;
    //write_serial_string("hi init_page 2\n");
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 2
    #endif
    // Map some pages in the kernel heap area.
    // Here we call get_page but not alloc_frame. This causes page_table_t's 
    // to be created where necessary. We can't allocate frames yet because they
    // they need to be identity mapped first below, and yet we can't increase
    // placement_address between identity mapping and enabling the heap!
    uint32_t i = 0;
    for (i = KHEAP_START; i < (KHEAP_START+KHEAP_INITIAL_SIZE); i += 0x1000)
        get_page(i, 1, kernel_directory);
        
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 3
    #endif
    // We need to identity map (phys addr = virt addr) from
    // 0x0 to the end of used memory, so we can access this
    // transparently, as if paging wasn't enabled.
    // NOTE that we use a while loop here deliberately.
    // inside the loop body we actually change placement_address
    // by calling kmalloc(). A while loop causes this to be
    // computed on-the-fly rather than once at the start.
    // Allocate a lil' bit extra so the kernel heap can be
   // initialised properly.
     uint32_t pa = 0x400000;
     if ((placement_address+0x1000) > pa)
     {
       pa =  (placement_address+0x1000);
     }
    i = 0;
    write_serial_string("placement_address+0x1000: 0x%X\n",placement_address+0x1000 );
    while (i < pa ) //placement_address+0x1000)
    {
      #ifdef PAGING_PERMISSIONS
      if ( i >=  &_stext && i <= &_etext )
      {
        // Kernel code is readable but not writeable from userspace.
        alloc_frame( get_page(i, 1, kernel_directory), 0, 1);
        
      }
      else if ( i >=  &_srodata && i <= &_erodata )
      {
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);
      }
      else if ( i >=  &_sdata && i <= &_edata )
      {
        alloc_frame( get_page(i, 1, kernel_directory), 0, 1);
      }
      else if ( i >=  &_sbss && i <= &_ebss )
      {
        alloc_frame( get_page(i, 1, kernel_directory), 0, 1);
      }
      else
      {
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);
      }
      #else
      // Kernel code is readable but not writeable from userspace.
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);
      #endif
        i += 0x1000;
    }
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 4
    write_serial_string("pa: 0x%X\n", pa);
    #endif
    // Now allocate those pages we mapped earlier.
    for (i = KHEAP_START; i < (KHEAP_START+KHEAP_INITIAL_SIZE); i += 0x1000)
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);
    
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 5
    #endif   
    // Before we enable paging, we must register our page fault handler.
    //register_interrupt_handler(14, page_fault); // TODO: should this be &page_fault ?
    register_interrupt_handler(14, &page_fault);
    //write_serial_string("hi init_page 3\n");
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 6
    #endif
    // Now, enable paging!
    switch_page_directory(kernel_directory);
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 7
    #endif
    //enable_interrupts();
    enablePaging();
    //write_serial_string("hi init_page 4\n");
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 8
    #endif
    // Initialise the kernel heap.
    kheap = create_heap(KHEAP_START, KHEAP_START+KHEAP_INITIAL_SIZE, 0xCFFFF000, 0, 0);
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 9
    #endif
    current_directory = clone_directory(kernel_directory);
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 10
    #endif
    switch_page_directory(current_directory);
    #ifdef DEBUG_PAGING_C
    write_serial_string("initialise_paging: %i\n", print_debug++); // 11
    #endif
}



void switch_page_directory(page_directory_t *dir)
{    
    #ifdef DEBUG_PAGING_C
    int print_debug = 1;
    write_serial_string("switch_page_directory: %i\n", print_debug++); // 1
    #endif
    current_directory = dir;
    //write_serial_string("hi switch_page_directory 1\n");
    #ifdef DEBUG_PAGING_C
    write_serial_string("switch_page_directory: %i\n", print_debug++); // 2
    #endif
    write_serial_string("&dir->tablesPhysical: 0x%X\ndir->physicalAddr: 0x%X\n",&dir->tablesPhysical, dir->physicalAddr);  // LAST KNOWN LINE PRINTED
    asm volatile("mov %0, %%cr3":: "r"(dir->physicalAddr));
    //uint32_t cr0;
    //write_serial_string("hi switch_page_directory 2\n");
    //asm volatile("mov %%cr0, %0": "=r"(cr0));
    /*asm volatile("mov %cr0, %eax;"
    "or $0x80000000, %eax;"
    "mov %eax, %cr0;");*/
    //cr0 |= 0x80000000; // Enable paging!
    //write_serial_string("hi switch_page_directory 3\n");
    //asm volatile("mov %0, %%cr0":: "r"(cr0));  // <---- problem lies here!!! TODO: fix me
    //write_serial_string("hi switch_page_directory 4\n");
    #ifdef DEBUG_PAGING_C
    write_serial_string("switch_page_directory: %i\n", print_debug++); // 3
    #endif
}


page_t *get_page(uint32_t address, int make, page_directory_t *dir)
{
    // Turn the address into an index.
    address /= 0x1000;
    // Find the page table containing this address.
    uint32_t table_idx = address / 1024;
    if (dir->tables[table_idx]) // If this table is already assigned
    {
        return &dir->tables[table_idx]->pages[address%1024];
    }
    else if(make)
    {
        uint32_t tmp;
        dir->tables[table_idx] = (page_table_t*)kmalloc_ap(sizeof(page_table_t), &tmp);
        memset(dir->tables[table_idx], 0, 0x1000);
        dir->tablesPhysical[table_idx] = tmp | 0x7; // PRESENT, RW, US.
        return &dir->tables[table_idx]->pages[address%1024];
    }
    else
    {
        return 0;
    }
}


void page_fault(registers_t * regs)
{
    // A page fault has occurred.
    // The faulting address is stored in the CR2 register.
    uint32_t faulting_address;
    asm volatile("mov %%cr2, %0" : "=r" (faulting_address));
    
    // The error code gives us details of what happened.
    int present   = !(regs->err_code & 0x1); // Page not present
    int rw = regs->err_code & 0x2;           // Write operation?
    int us = regs->err_code & 0x4;           // Processor was in user-mode?
    int reserved = regs->err_code & 0x8;     // Overwritten CPU-reserved bits of page entry?
    int id = regs->err_code & 0x10;          // Caused by an instruction fetch?

    // Output an error message.
    printf("Page fault! ( \n");
    write_serial_string("Page fault! ( \n");
    if (present) 
    {
      printf("The fault was caused by a not-present page.\n");
      write_serial_string("The fault was caused by a not-present page.\n");
      }
      else
      {
      printf("The fault was caused by a page-level protection violation.\n");
      write_serial_string("The fault was caused by a page-level protection violation.\n");
      }
    if (rw) 
    {
    printf("The access causing the fault was a write. i.e:read-only \n");
    write_serial_string("The access causing the fault was a write. i.e:read-only \n");
    }
    else
    {
    printf("The access causing the fault was a read.\n");
    write_serial_string("The access causing the fault was a read.\n");
    }
    if (us) 
    {
    printf("The access causing the fault originated when the processor was executing in user mode.\n");
    write_serial_string("The access causing the fault originated when the processor was executing in user mode.\n");
    }
    else
    {
    printf("The access causing the fault originated when the processor was executing in supervisor mode.\n");
    write_serial_string("The access causing the fault originated when the processor was executing in supervisor mode.\n");
    }
    if (reserved) {printf("The fault was caused by reserved bits being overwritten \n");write_serial_string("The fault was caused by reserved bits being overwritten \n");}
    if (id) {printf("The access causing the fault was an instruction fetch \n");write_serial_string("The access causing the fault was an instruction fetch \n");}
    write_serial_string(") at 0x%X\n", faulting_address);
    printf(") at 0x%X\n", faulting_address);
    
    PANIC("Page fault");
}

static page_table_t *clone_table(page_table_t *src, uint32_t *physAddr)
{
    // Make a new page table, which is page aligned.
    
    page_table_t *table = (page_table_t*)kmalloc_ap(sizeof(page_table_t), physAddr);
    // Ensure that the new table is blank.
    memset(table, 0, sizeof(page_directory_t));

    // For every entry in the table...
    int i;
    for (i = 0; i < 1024; i++)
    {
        // If the source entry has a frame associated with it...
        if (src->pages[i].frame)
        {
            // Get a new frame.
            alloc_frame(&table->pages[i], 0, 0);
            // Clone the flags from source to destination.
            if (src->pages[i].present) table->pages[i].present = 1;
            if (src->pages[i].rw) table->pages[i].rw = 1;
            if (src->pages[i].user) table->pages[i].user = 1;
            if (src->pages[i].accessed) table->pages[i].accessed = 1;
            if (src->pages[i].dirty) table->pages[i].dirty = 1;
            // Physically copy the data across. This function is in process.s.
            copy_page_physical(src->pages[i].frame*0x1000, table->pages[i].frame*0x1000);
        }
    }
    return table;
}

page_directory_t *clone_directory(page_directory_t *src)
{
    #ifdef DEBUG_PAGING_C
    int print_debug = 1;
    write_serial_string("clone_directory: %i\n", print_debug++); // 1
    #endif
    uint32_t phys;
    // Make a new page directory and obtain its physical address.
    page_directory_t *dir = (page_directory_t*)kmalloc_ap(sizeof(page_directory_t), &phys);
    // Ensure that it is blank.
    memset(dir, 0, sizeof(page_directory_t));

    // Get the offset of tablesPhysical from the start of the page_directory_t structure.
    uint32_t offset = (uint32_t)dir->tablesPhysical - (uint32_t)dir;

    // Then the physical address of dir->tablesPhysical is:
    dir->physicalAddr = phys + offset;
    
    #ifdef DEBUG_PAGING_C
    write_serial_string("clone_directory: %i\n", print_debug++); // 2
    write_serial_string("(uint32_t)dir: 0x%X\n", (uint32_t)dir); 
    write_serial_string("(uint32_t)dir->tablesPhysical: 0x%X\n", (uint32_t)dir->tablesPhysical); 
    write_serial_string("(uint32_t)dir->tablesPhysical - (uint32_t)dir: 0x%X\n", (uint32_t)dir->tablesPhysical - (uint32_t)dir); 
    write_serial_string("phys: 0x%X\n", phys); 
    write_serial_string("offset: 0x%X\n",offset); 
    write_serial_string("phys + offset: 0x%X\n", phys + offset); 
    #endif
    // Go through each page table. If the page table is in the kernel directory, do not make a new copy.
    int i;
    for (i = 0; i < 1024; i++)
    {
        if (!src->tables[i])
            continue;

        if (kernel_directory->tables[i] == src->tables[i])
        {
            // It's in the kernel, so just use the same pointer.
            dir->tables[i] = src->tables[i];
            dir->tablesPhysical[i] = src->tablesPhysical[i];
        }
        else
        {
            // Copy the table.
            uint32_t phys;
            dir->tables[i] = clone_table(src->tables[i], &phys);
            dir->tablesPhysical[i] = phys | 0x07;
        }
    }
    return dir;
}

void * get_physaddr(void * virtualaddr)
{
    unsigned long pdindex = (unsigned long)virtualaddr >> 22;
    unsigned long ptindex = (unsigned long)virtualaddr >> 12 & 0x03FF;
 
    unsigned long * pd = (unsigned long *)0xFFFFF000;
    // Here you need to check whether the PD entry is present.
 
    unsigned long * pt = ((unsigned long *)0xFFC00000) + (0x400 * pdindex);
    // Here you need to check whether the PT entry is present.
 
    return (void *)((pt[ptindex] & ~0xFFF) + ((unsigned long)virtualaddr & 0xFFF));
}

void map_page(void * physaddr, void * virtualaddr, unsigned int flags)
{
    // Make sure that both addresses are page-aligned.
 
    unsigned long pdindex = (unsigned long)virtualaddr >> 22;
    unsigned long ptindex = (unsigned long)virtualaddr >> 12 & 0x03FF;
 
    unsigned long * pd = (unsigned long *)0xFFFFF000;
    // Here you need to check whether the PD entry is present.
    // When it is not present, you need to create a new empty PT and
    // adjust the PDE accordingly.
 
    unsigned long * pt = ((unsigned long *)0xFFC00000) + (0x400 * pdindex);
    // Here you need to check whether the PT entry is present.
    // When it is, then there is already a mapping present. What do you do now?
 
    pt[ptindex] = ((unsigned long)physaddr) | (flags & 0xFFF) | 0x01; // Present
 
    // Now you need to flush the entry in the TLB
    // or you might not notice the change.
}
