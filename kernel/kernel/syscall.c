// syscall.c -- Defines the implementation of a system call system.
//              Written for JamesM's kernel development tutorials.

#include <kernel/syscall.h>
#include <irqs/isr.h>
#include <kernel/tty.h>
#include <kernel/com_one.h>
#include <kernel/kheap.h>
#include <unistd.h>
#include <stdint.h>

#define DEBUG_SYSCALL 1

static void syscall_handler(registers_t *regs);

#ifndef NEWLIB_SYSCALLS
DEFN_SYSCALL1(monitor_write, 0, const char*);
DEFN_SYSCALL1(terminal_writestring, 1, const char*);
DEFN_SYSCALL1(kmalloc, 2, uint32_t);
DEFN_SYSCALL2(kmalloc_p, 3, uint32_t, uint32_t *);
int syscall_write(int file, char *ptr, int len);
static void *syscalls[5] =
{
    &monitor_write,
    &terminal_writestring,
    &kmalloc,
    &kmalloc_p,
    &syscall_write,
};
uint32_t num_syscalls = 5;

#else
DEFN_SYSCALL1(syscall_monitor_write, 0, const char*);
DEFN_SYSCALL1(syscall_terminal_writestring, 1, const char*);
DEFN_SYSCALL1(syscall_kmalloc, 2, uint32_t);
DEFN_SYSCALL2(syscall_kmalloc_p, 3, uint32_t, uint32_t *);
int syscall_write(int file, char *ptr, int len);
static void *syscalls[5] =
{
    &monitor_write,
    &terminal_writestring,
    &kmalloc,
    &kmalloc_p,
    &syscall_write,
};
uint32_t num_syscalls = 5;
#endif
void initialise_syscalls()
{
#ifdef DEBUG_SYSCALL
int print_debug = 1;
write_serial_string("initialise_syscalls: %i\n", print_debug++); // 1
    #endif
    // Register our syscall handler.
    register_interrupt_handler (0x80, &syscall_handler);
    #ifdef DEBUG_SYSCALL
write_serial_string("initialise_syscalls: %i\n", print_debug++); // 2
    #endif
}

void syscall_handler(registers_t *regs)
{
    
    // Firstly, check if the requested syscall number is valid.
    // The syscall number is found in EAX.
    if (regs->eax >= num_syscalls)
        return;
    
    #ifdef DEBUG_SYSCALL
    int print_debug = 1;
    write_serial_string("syscall_handler: %i\n", print_debug++); // 1
    #endif
    // Get the required syscall location.
    void *location = syscalls[regs->eax];
    
    #ifdef DEBUG_SYSCALL

write_serial_string("syscall_handler: %i\n", print_debug++); // 1
write_serial_string("location: 0x%X\n", location); // 1
    #endif
    // We don't know how many parameters the function wants, so we just
    // push them all onto the stack in the correct order. The function will
    // use all the parameters it wants, and we can pop them all back off afterwards.
    int ret;
    asm volatile (" \
      push %1; \
      push %2; \
      push %3; \
      push %4; \
      push %5; \
      call *%6; \
      pop %%ebx; \
      pop %%ebx; \
      pop %%ebx; \
      pop %%ebx; \
      pop %%ebx; \
    " : "=a" (ret) : "r" (regs->edi), "r" (regs->esi), "r" (regs->edx), "r" (regs->ecx), "r" (regs->ebx), "r" (location));
    regs->eax = ret;
}

int syscall_write(int file, char *ptr, int len){
  if (file == STDOUT_FILENO)
  {
    
    return write_serial_string("%s\n", ptr);
  }
  else if (file == STDERR_FILENO)
  {
    return -1;
  }
  else
  {
    return -1;
  }
  return 0;
}
