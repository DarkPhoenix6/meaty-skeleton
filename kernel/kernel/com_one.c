#include <ctype.h>
#include <kernel/com_one.h>
#include <irqs/inline_assembly.h>
#include <stddef.h>
#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>


#define PORT 0x3f8   /* COM1 */
 
void init_serial() {
   outb(PORT + 1, 0x00);    // Disable all interrupts
   outb(PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
   outb(PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
   outb(PORT + 1, 0x00);    //                  (hi byte)
   outb(PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
   outb(PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
   outb(PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set
}

int serial_received() {
   return inb(PORT + 5) & 1;
}
 
char read_serial() {
   while (serial_received() == 0);
 
   return inb(PORT);
}

int is_transmit_empty() {
   return inb(PORT + 5) & 0x20;
}
 
void write_serial(char a) {
   while (is_transmit_empty() == 0);
   
   outb(PORT,a);
}

char* convert_s(int num, int base)
{
  //memory leak ?
  //const char* str = (char*) 67;
  char str[67];
  char* str_ptr = str;
  itoa(num, str_ptr, base);
  return str_ptr;
}

char* uconvert_s(unsigned int num, int base)
{
  //memory leak ?
  //const char* str = (char*) 67;
  //const char* str[67];
  char str[67];
  char* str_ptr = str;
  utoa(num, str_ptr, base);
  return str_ptr;
}


static bool serial_out(const char* data, size_t length) {
	const unsigned char* bytes = (const unsigned char*) data;
	for (size_t i = 0; i < length; i++)
		write_serial(bytes[i]);
	return true;
}

int write_serial_string(const char* restrict format, ...) {
	va_list parameters;
	va_start(parameters, format);

	int written = 0;

	while (*format != '\0') {
		size_t maxrem = INT_MAX - written;

		if (format[0] != '%' || format[1] == '%') {
			if (format[0] == '%')
				format++;
			size_t amount = 1;
			while (format[amount] && format[amount] != '%')
				amount++;
			if (maxrem < amount) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!serial_out(format, amount))
				return -1;
			format += amount;
			written += amount;
			continue;
		}

		const char* format_begun_at = format++;

		if (*format == 'c') {
			format++;
			char c = (char) va_arg(parameters, int /* char promotes to int */);
			if (!maxrem) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!serial_out(&c, sizeof(c)))
				return -1;
			written++;
		} else if (*format == 's') {
			format++;
			const char* str = va_arg(parameters, const char*);
			size_t len = strlen(str);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
     
			if (!serial_out(str, len))
				return -1;
			written += len;
		} else if (*format == 'i' || *format == 'd') {
       format++;
       //char* str2 = (char*) 15;
       int num = (int) va_arg(parameters, int);
       //itoa(num, str2, 10);
       const char* str3 = convert_s(num,10);
       /*if (num == 55)
         puts("true");*/
       //size_t w = position_of_null(str3);
       size_t w = write_serial_string("%s",str3);
       if (w < 0)
       {
         // TODO: Set errno to Error.
				  return -1;
       } else if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          /*if (!serial_out(str3, w))
				    return -1;*/
          written += w;
        }
        
      } else if (*format == 'o') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       const char* str3 = uconvert_s(num,8);
       size_t w = strlen(str3);
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          if (!serial_out(str3, w))
				    return -1;
          written += w;
        }
    } else if (*format == 'u') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       const char* str3 = uconvert_s(num,10);
       size_t w = strlen(str3);
       //size_t w = write_serial_string(str3);
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          
          if (!serial_out(str3, w))
				    return -1;
          written += w;
        }
    } else if (*format == 'x') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       const char* str3 = uconvert_s(num,16);
       size_t w = strlen(str3);
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          if (!serial_out(str3, w))
				    return -1;
          written += w;
        }
    } else if (*format == 'X') {
       format++;
       unsigned int num = (unsigned int) va_arg(parameters, unsigned int);
       // TODO: Make const
       char* str3 = uconvert_s(num,16);
       
       int i = 0;
       while (str3[i])
       {
           str3[i] = (char) toupper(str3[i]);
           i++;
       }
       size_t w = strlen(str3);
       const char* str4 = str3;
        if ( maxrem < w) 
       {
         // TODO: Set errno to EOVERFLOW.
				  return -1;
			 } else
        {
          if (!serial_out(str4, w))
				    return -1;
          written += w;
        }
    } else {
			format = format_begun_at;
			size_t len = strlen(format);
			if (maxrem < len) {
				// TODO: Set errno to EOVERFLOW.
				return -1;
			}
			if (!serial_out(format, len))
				return -1;
			written += len;
			format += len;
		}
	}

	va_end(parameters);
	return written;
}