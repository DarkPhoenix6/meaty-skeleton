#include <CMOS/pit.h>
uint32_t RTC_frequency = 32768;

uint8_t read_register_c()
{
    outb(0x70, 0x0C);	// select register C
    return inb(0x71);	
}

void turn_on_IRQ8()
{
    char clear_c;
    disable_interrupts();// disable interrupts
    outb(0x70, 0x8B);		// select register B, and disable NMI
    char prev=inb(0x71);	// read the current value of register B
    outb(0x70, 0x8B);		// set the index again (a read will reset the index to register D)
    outb(0x71, prev | 0x40);	// write the previous value ORed with 0x40. This turns on bit 6 of register B
    clear_c = read_register_c(); // clearing interupts
    enable_interrupts();
    
}

void change_interrupt_rate(int rate)
{
    rate &= 0x0f;       // rate must be above 2 and not over 15
    RTC_frequency = RTC_DEFAULT_FREQ >> (rate-1);
    disable_interrupts();// disable interrupts
    outb(0x70, 0x8A);		// set index to register A, disable NMI
    char prev=inb(0x71);	// get initial value of register A
    outb(0x70, 0x8A);		// reset index to A
    outb(0x71, (prev & 0xF0) | rate); //write only our rate to A. Note, rate is the bottom 4 bits.
    enable_interrupts();
}
