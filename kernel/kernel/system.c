#include <stdint.h>
#include <kernel/system.h>
#include <stdio.h>
#include <kernel/kheap.h>
extern void panic(const char *message, const char *file, uint32_t line)
{
    // We encountered a massive problem and have to stop.
    asm volatile("cli"); // Disable interrupts.

    printf("PANIC(%s) at %s:%u\n", message, file, line);

    // Halt by going into an infinite loop.
    for(;;);
}

extern void panic_assert(const char *file, uint32_t line, const char *desc)
{
    // An assertion failed, and we have to panic.
    asm volatile("cli"); // Disable interrupts.

    printf("ASSERTION-FAILED(%s) at %s:%u\n", desc, file, line);
    
    // Halt by going into an infinite loop.
    for(;;);
}

// Copy len bytes from src to dest.
void kmemcpy(uint8_t *dest, const uint8_t *src, uint32_t len)
{
    const uint8_t *sp = (const uint8_t *)src;
    uint8_t *dp = (uint8_t *)dest;
    for(; len != 0; len--) *dp++ = *sp++;
}

// void * kmemsetw(uint16_t * ptr, uint16_t wc, size_t num)
void kmemsetw(uint16_t * ptr, uint16_t wc, size_t num)
{
  uint16_t * ptr_dp = (uint16_t *) ptr;
  for (size_t i = 0; i < num; i++)
		ptr_dp[i] = (uint16_t) wc;
	//return ptr;

} 

/*
brk() and sbrk() change the location of the program break, which defines the end of the process's data segment (i.e., the program break is the first location after the end of the uninitialized data segment). Increasing the program break has the effect of allocating memory to the process; decreasing the break deallocates memory.
*/
/*
increments the program's data space by increment bytes. Calling sbrk() with an increment of 0 can be used to find the current location of the program break.

For now just call kmalloc()
*/
 /*void *sbrk(size_t size){
  
  uint32_t stuff = kmalloc((uint32_t) size);
  uint32_t * my_addr = stuff;
  return (void *) my_addr;

}
*/
/* sets the end of the data segment to the value specified by addr, when that value is reasonable, the system has enough memory, and the process does not exceed its maximum data size (see setrlimit(2)).
  
  For now just call kfree()
*/
/*
int brk(const void *addr )
{
  
  return -1; /// error
}
*/
