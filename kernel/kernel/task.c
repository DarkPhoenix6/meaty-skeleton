// 
// task.c - Implements the functionality needed to multitask.
//          Written for JamesM's kernel development tutorials.
//

#include <kernel/task.h>
#include <kernel/paging.h>
#include <kernel/kheap.h>
#include <irqs/inline_assembly.h>
#include <stdint.h>
#include <kernel/system.h>
#define DEBUG_TASK 1
//#undef DEBUG_TASK
#ifdef DEBUG_TASK
#include <kernel/com_one.h>
#include <stdio.h>
#endif
// The currently running task.
volatile task_t *current_task;

// The start of the task linked list.
volatile task_t *ready_queue;

// from process.s
extern void perform_task_switch(uint32_t, uint32_t, uint32_t, uint32_t);

// Some externs are needed to access members in paging.c...
extern page_directory_t *kernel_directory;
extern page_directory_t *current_directory;
extern void alloc_frame(page_t*,int,int);
extern uint32_t initial_esp;
extern uint32_t read_eip();

// The next available process ID.
uint32_t next_pid = 1;

/*
static Task *runningTask;
static Task mainTask;
static Task otherTask;
 
static void otherMain() {
    printf("Hello multitasking world!"); 
    yield();
}
 
void initTasking() {
    // Get EFLAGS and CR3
    asm volatile("movl %%cr3, %%eax; movl %%eax, %0;":"=m"(mainTask.regs.cr3)::"%eax");
    asm volatile("pushfl; movl (%%esp), %%eax; movl %%eax, %0; popfl;":"=m"(mainTask.regs.eflags)::"%eax");
 
    createTask(&otherTask, otherMain, mainTask.regs.eflags, (uint32_t*)mainTask.regs.cr3);
    mainTask.next = &otherTask;
    otherTask.next = &mainTask;
 
    runningTask = &mainTask;
}
 
void createTask(Task *task, void (*main)(), uint32_t flags, uint32_t *pagedir) {
    task->regs.eax = 0;
    task->regs.ebx = 0;
    task->regs.ecx = 0;
    task->regs.edx = 0;
    task->regs.esi = 0;
    task->regs.edi = 0;
    task->regs.eflags = flags;
    task->regs.eip = (uint32_t) main;
    task->regs.cr3 = (uint32_t) pagedir;
    task->regs.esp = (uint32_t) allocPage() + 0x1000; // Not implemented here
    task->next = 0;
}
 
void yield() {
    Task *last = runningTask;
    runningTask = runningTask->next;
    switchTask(&last->regs, &runningTask->regs);
}
*/
void initialise_tasking()
{
    #ifdef DEBUG_TASK
    int print_debug = 1;
    write_serial_string("initialise_tasking: %i\n", print_debug++); // 1
    #endif
    // Rather important stuff happening, no interrupts please!
    bool interrupts_en = are_interrupts_enabled();
    //disable_interrupts();
    //NMI_disable();
    //NMI_and_interrupts_disable();
    #ifdef DEBUG_TASK
    write_serial_string("initialise_tasking: %i\n", print_debug++); // 2
    #endif
    // Relocate the stack so we know where it is.
    move_stack((void*)0xE0000000, 0x2000);
    #ifdef DEBUG_TASK
    write_serial_string("initialise_tasking: %i\n", print_debug++); // 3
    #endif
    // Initialise the first task (kernel task)
    current_task = ready_queue = (task_t*)kmalloc(sizeof(task_t));
    current_task->id = next_pid++;
    current_task->esp = current_task->ebp = 0;
    current_task->eip = 0;
    current_task->page_directory = current_directory;
    current_task->next = 0;
    current_task->kernel_stack = kmalloc_a(KERNEL_STACK_SIZE);
    #ifdef DEBUG_TASK
    write_serial_string("initialise_tasking: %i\n", print_debug++); // 4
    #endif
    // Reenable interrupts.
    if (interrupts_en == true)
    {
      //enable_interrupts();
      //NMI_enable();
      NMI_and_interrupts_enable();
    }
    #ifdef DEBUG_TASK
    write_serial_string("initialise_tasking: %i\n", print_debug++); // 5
    #endif
}

void move_stack(void *new_stack_start, uint32_t size)
{
  uint32_t i;
  #ifdef DEBUG_TASK
    int print_debug = 1;
    write_serial_string("move_stack: %i\n", print_debug++); // 1
    write_serial_string("size: 0x%X\n", size);
    #endif
  // Allocate some space for the new stack.
  for( i = (uint32_t)new_stack_start;
       i >= ((uint32_t)new_stack_start-size);
       i -= 0x1000)
  {
    // General-purpose stack is in user-mode.
    alloc_frame( get_page(i, 1, current_directory), 0 /* User mode */, 1 /* Is writable */ );
  }
  #ifdef DEBUG_TASK

    write_serial_string("move_stack: %i\n", print_debug++); // 2
    #endif
  // Flush the TLB by reading and writing the page directory address again.
  uint32_t pd_addr;
  asm volatile("mov %%cr3, %0" : "=r" (pd_addr));
  asm volatile("mov %0, %%cr3" : : "r" (pd_addr));

  #ifdef DEBUG_TASK
    write_serial_string("move_stack: %i\n", print_debug++); // 3
    #endif
  // Old ESP and EBP, read from registers.
 
  uint32_t old_base_pointer;  asm volatile("mov %%ebp, %0" : "=r" (old_base_pointer));
  uint32_t old_stack_pointer; asm volatile("mov %%esp, %0" : "=r" (old_stack_pointer));

  // Offset to add to old stack addresses to get a new stack address.
  uint32_t offset            = (uint32_t)new_stack_start - initial_esp;

  // New ESP and EBP.
  uint32_t new_stack_pointer = old_stack_pointer + offset;
  uint32_t new_base_pointer  = old_base_pointer  + offset;

    #ifdef DEBUG_TASK
    write_serial_string("size: 0x%X\n", size);
    write_serial_string("move_stack: %i\n", print_debug++); // 4
    write_serial_string("(uint32_t)new_stack_start: 0x%X\n", (uint32_t)new_stack_start);
    write_serial_string("initial_esp: 0x%X\n", initial_esp);
    write_serial_string("offset: 0x%X\n", offset);
    write_serial_string("new_stack_pointer: 0x%X\n", new_stack_pointer);
    write_serial_string("old_stack_pointer: 0x%X\n", old_stack_pointer);
    write_serial_string("new_base_pointer: 0x%X\n", new_base_pointer);
    write_serial_string("old_base_pointer: 0x%X\n", old_base_pointer);
    write_serial_string("initial_esp-old_stack_pointer: 0x%X\n", initial_esp-old_stack_pointer);
    #endif
  // Copy the stack.
    kmemcpy((void*)new_stack_pointer, (void*)old_stack_pointer, initial_esp-old_stack_pointer);

    #ifdef DEBUG_TASK

    write_serial_string("move_stack: %i\n", print_debug++); // 5
    #endif

  // Backtrace through the original stack, copying new values into
  // the new stack.  
  for(i = (uint32_t)new_stack_start; i > (uint32_t)new_stack_start-size; i -= 4)
  {
    uint32_t tmp = * (uint32_t*)i;
    // If the value of tmp is inside the range of the old stack, assume it is a base pointer
    // and remap it. This will unfortunately remap ANY value in this range, whether they are
    // base pointers or not.
    if (( old_stack_pointer < tmp) && (tmp < initial_esp))
    {
      tmp = tmp + offset;
      uint32_t *tmp2 = (uint32_t*)i;
      *tmp2 = tmp;
    }
  }
    #ifdef DEBUG_TASK

    write_serial_string("move_stack: %i\n", print_debug++); // 6
    #endif
  // Change stacks.
    
  // asm volatile("mov %0, %%esp" : : "r" (new_stack_pointer)); // This way seems fishy. The way that this is done the new base pointer value is found using an offset relative the new stack pointer...
  // asm volatile("mov %0, %%ebp" : : "r" (new_base_pointer));
  asm volatile("mov %0, %%ebp" : : "r" (new_base_pointer));
  asm volatile("mov %0, %%esp" : : "r" (new_stack_pointer));
    #ifdef DEBUG_TASK

    write_serial_string("move_stack: %i\n", print_debug++); // 7
    #endif
}

void switch_task()
{  
  
    // If we haven't initialised tasking yet, just return.
    if (!current_task)
        return;
    
    #ifdef DEBUG_TASK
    int print_debug = 1;
    write_serial_string("switch_task: %i\n", print_debug++); // 1
    #endif
    // Read esp, ebp now for saving later on.
    uint32_t esp, ebp, eip;
    asm volatile("mov %%esp, %0" : "=r"(esp));
    asm volatile("mov %%ebp, %0" : "=r"(ebp));
    
    #ifdef DEBUG_TASK

    write_serial_string("switch_task: %i\n", print_debug++); // 2
    #endif
    // Read the instruction pointer. We do some cunning logic here:
    // One of two things could have happened when this function exits - 
    //   (a) We called the function and it returned the EIP as requested.
    //   (b) We have just switched tasks, and because the saved EIP is essentially
    //       the instruction after read_eip(), it will seem as if read_eip has just
    //       returned.
    // In the second case we need to return immediately. To detect it we put a dummy
    // value in EAX further down at the end of this function. As C returns values in EAX,
    // it will look like the return value is this dummy value! (0x12345).
    eip = read_eip();
    #ifdef DEBUG_TASK

    write_serial_string("switch_task: %i\n", print_debug++); // 3
    write_serial_string("eip: 0x%X, ebp: 0x%X, esp: 0x%X\n", eip, ebp, esp); // 3
    #endif
    // Have we just switched tasks?
    if (eip == 0x12345)
        return;
    
    #ifdef DEBUG_TASK

    write_serial_string("switch_task: %i\n", print_debug++); // 4
    #endif
    // No, we didn't switch tasks. Let's save some register values and switch.
    current_task->eip = eip;
    current_task->esp = esp;
    current_task->ebp = ebp;
    
    // Get the next task to run.
    current_task = current_task->next;
    
    #ifdef DEBUG_TASK

    write_serial_string("switch_task: %i\n", print_debug++); // 5
    #endif
    // If we fell off the end of the linked list start again at the beginning.
    if (!current_task) current_task = ready_queue;

    eip = current_task->eip;
    esp = current_task->esp;
    ebp = current_task->ebp;

    // Make sure the memory manager knows we've changed page directory.
    current_directory = current_task->page_directory;
    
    // Change our kernel stack over.
    set_kernel_stack(current_task->kernel_stack+KERNEL_STACK_SIZE);
    #ifdef DEBUG_TASK

    write_serial_string("switch_task: %i\n", print_debug++); // 6
    #endif
    // Here we:
    // * Stop interrupts so we don't get interrupted.
    // * Temporarily puts the new EIP location in ECX.
    // * Loads the stack and base pointers from the new task struct.
    // * Changes page directory to the physical address (physicalAddr) of the new directory.
    // * Puts a dummy value (0x12345) in EAX so that above we can recognise that we've just
    //   switched task.
    // * Restarts interrupts. The STI instruction has a delay - it doesn't take effect until after
    //   the next instruction.
    // * Jumps to the location in ECX (remember we put the new EIP in there).
   /* asm volatile("         \
      cli;                 \
      mov %0, %%ecx;       \
      mov %1, %%esp;       \
      mov %2, %%ebp;       \
      mov %3, %%cr3;       \
      mov $0x12345, %%eax; \
      sti;                 \
      jmp *%%ecx           "
                 : : "r"(eip), "r"(esp), "r"(ebp), "r"(current_directory->physicalAddr));
      */       
      perform_task_switch(eip, current_directory->physicalAddr, ebp, esp);
      
      #ifdef DEBUG_TASK

    //write_serial_string("switch_task: %i\n", print_debug++); // 7
    #endif
}

int fork()
{    
    #ifdef DEBUG_TASK
    int print_debug = 1;
    write_serial_string("fork: %i\n", print_debug++); // 1
    #endif
    // We are modifying kernel structures, and so cannot
    bool interrupts_en = are_interrupts_enabled();
    disable_interrupts();
    NMI_disable();
    #ifdef DEBUG_TASK
    write_serial_string("fork: %i\n", print_debug++); // 2
    #endif
    // Take a pointer to this process' task struct for later reference.
    task_t *parent_task = (task_t*)current_task;
    #ifdef DEBUG_TASK
    write_serial_string("fork: %i\n", print_debug++); // 3
    #endif
    // Clone the address space.
    page_directory_t *directory = clone_directory(current_directory);
    #ifdef DEBUG_TASK
    write_serial_string("fork: %i\n", print_debug++); // 4
    #endif
    // Create a new process.
    task_t *new_task = (task_t*)kmalloc(sizeof(task_t));
    #ifdef DEBUG_TASK
    write_serial_string("fork: %i\n", print_debug++); // 5
    #endif
    new_task->id = next_pid++;
    new_task->esp = new_task->ebp = 0;
    new_task->eip = 0;
    new_task->page_directory = directory;
    current_task->kernel_stack = kmalloc_a(KERNEL_STACK_SIZE);
    new_task->next = 0;
    #ifdef DEBUG_TASK
    write_serial_string("new_task->page_directory: 0x%X\n", new_task->page_directory);
     write_serial_string("new_task->page_directory->physicalAddr: 0x%X\n", new_task->page_directory->physicalAddr);
    write_serial_string("current_task->kernel_stack: 0x%X\n", current_task->kernel_stack);
    write_serial_string("fork: %i\n", print_debug++); // 6
    #endif
    // Add it to the end of the ready queue.
    // Find the end of the ready queue...
    task_t *tmp_task = (task_t*)ready_queue;
    while (tmp_task->next)
        tmp_task = tmp_task->next;
    // ...And extend it.
    tmp_task->next = new_task;
    #ifdef DEBUG_TASK
    write_serial_string("fork: %i\n", print_debug++); // 7
    #endif
    // This will be the entry point for the new process.
    uint32_t eip = read_eip();
    #ifdef DEBUG_TASK
    write_serial_string("fork: %i\n", print_debug++); // 8
    #endif

    // We could be the parent or the child here - check.
    if (current_task == parent_task)
    {
        // We are the parent, so set up the esp/ebp/eip for our child.
        uint32_t esp; asm volatile("mov %%esp, %0" : "=r"(esp));
        uint32_t ebp; asm volatile("mov %%ebp, %0" : "=r"(ebp));
        #ifdef DEBUG_TASK
        write_serial_string("eip: 0x%X, ebp: 0x%X, esp: 0x%X\n", eip, ebp, esp); // 3
        #endif
        new_task->esp = esp;
        new_task->ebp = ebp;
        new_task->eip = eip;
        if (interrupts_en == true)
        {
          enable_interrupts();
          NMI_enable();
        }
        
         #ifdef DEBUG_TASK
          write_serial_string("fork parent: %i\n", print_debug++); // 9
         #endif
        return new_task->id;
    }
    else
    {
        #ifdef DEBUG_TASK
          write_serial_string("fork child: %i\n", print_debug++); // 9
         #endif
        // We are the child.
        return 0;    // TODO: Fix Problem here
    }

}

int getpid()
{
    return current_task->id;
}

void switch_to_user_mode()
{
    // Set up our kernel stack.
    set_kernel_stack(current_task->kernel_stack+KERNEL_STACK_SIZE);
    
    // Set up a stack structure for switching to user mode.
    asm volatile("  \
      cli; \
      mov $0x23, %ax; \
      mov %ax, %ds; \
      mov %ax, %es; \
      mov %ax, %fs; \
      mov %ax, %gs; \
                    \
       \
      mov %esp, %eax; \
      pushl $0x23; \
      pushl %esp; \
      pushf; \
      pop %edx ; \
      or $0x200, %edx; \
      pushl %edx ; \
      pushl $0x1B; \
      push $1f; \
      iret; \
    1: \
      "); 
      //pop %eax ; Get EFLAGS back into EAX. The only way to read EFLAGS is to pushf then pop.
//or %eax, $0x200 ; Set the IF flag.
//push %eax ; Push the new EFLAGS value back onto the stack.
}
