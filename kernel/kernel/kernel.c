#include <kernel/kernel.h>
#ifdef MULTIBOOT_TEST
static multiboot_info_t *mbi;
#endif
#undef QSORT_TEST
// #define QSORT_TEST 1
extern uint32_t placement_address;
uint32_t initial_esp;
uint32_t Timer_val;
// from linker_script
extern uint32_t endkernel;


void _init_interupts()
{
  NMI_enable();
  io_wait();
  enable_interrupts();
  io_wait();
}
void more_tests()
{
  _init_interupts();
  #ifdef PAGE_TEST
  disable_interrupts();
  initialise_paging();
  _init_interupts();
  #endif
  printf("Hello, paging world!\n");
#ifdef PAGE_TEST
  uint32_t *ptr = (uint32_t*)0xA0000000;
    uint32_t do_page_fault = *ptr; 
    #endif
}

void init_stuff()
{
#ifdef PAGE_TEST
  init_descriptor_tables();
  init_serial();
	terminal_initialize();
 #endif
  more_tests();
 _init_interupts();
}

void do_tests()
{
  
  bool irqs_enabled;
  irqs_enabled = are_interrupts_enabled();
	//printf(irqs_enabled);
  NMI_enable();
  enable_interrupts();
  irqs_enabled = are_interrupts_enabled();
	//printf(irqs_enabled);
  uint16_t test_var = 22;
  printf("%s\n", btoa(check_apic()));
  write_serial_string("%s\n", btoa(check_apic()));
  print_date();
  printf("\n");
  printf("%u \n", test_var); 
  unsigned int test_int = 2222;
  printf("%u \n", test_int);
  printf("h");
  printf("\n");
  //printf('p');
  //printf((char*)4);
  //printf(itoa(4));
  printf("%s \n", "33");
  print_date();
  char* stu = (char*) 44;
  itoa(44, stu, 10);
  puts("33");
  puts(stu);
  char* s = (char*) 34;
  itoa(sizeof(char), s, 10);
  puts(s);
  itoa(sizeof(short), s, 10);
  puts(s);
  itoa(sizeof(int), s, 10);
  puts(s);
  itoa(sizeof(long), s, 10);
  puts(s);
  itoa(sizeof(long long), s, 10);
  puts(s);
  
  int d = 55;
  printf("%i\n", 3333); 
  printf("hi %d\n", d);
  itoa(d, stu, 10);
  printf(stu);
  printf("\n");
  printf("%o \n", 14);
  printf("%c \n", '2');
  printf("%s \n", "hello world!");
  printf("%x \n", 0xFFFF);
  printf("%X \n", 0xFFEF);
  printf("%%hi \n");
  char myin = getchar();
  write_serial('a');
  printf("%c \n", myin);
  write_serial_string("%c \n", (char)getchar());
  //write_serial(getchar());
  puts("hi");
  printf("hello");
  printf("%X \n", 0xF0EF);
  
  printf("\n");
  
  
   
}

int multiboot_test(unsigned long magic, unsigned long addr)
{


/* Am I booted by a Multiboot-compliant boot loader? */
       if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
         {
           write_serial_string("Invalid magic number: 0x%x\n", (unsigned) magic);
           printf ("Invalid magic number: 0x%x\n", (unsigned) magic);
           return 1;
         }
     
       /* Set MBI to the address of the Multiboot information structure. */
       mbi = (multiboot_info_t *) addr;
     
       /* Print out the flags. */
       write_serial_string("flags = 0x%x\n", (unsigned) mbi->flags);
       printf ("flags = 0x%x\n", (unsigned) mbi->flags);
     
       /* Are mem_* valid? */
       if (CHECK_FLAG (mbi->flags, 0))
       {
         write_serial_string("mem_lower = %uKB, mem_upper = %uKB\n",
                 (unsigned) mbi->mem_lower, (unsigned) mbi->mem_upper);
         printf ("mem_lower = %uKB, mem_upper = %uKB\n",
                 (unsigned) mbi->mem_lower, (unsigned) mbi->mem_upper);
         }
     
       /* Is boot_device valid? */
       if (CHECK_FLAG (mbi->flags, 1))
       {
      write_serial_string("boot_device = 0x%x\n", (unsigned) mbi->boot_device);
         printf ("boot_device = 0x%x\n", (unsigned) mbi->boot_device);
       }
       /* Is the command line passed? */
       if (CHECK_FLAG (mbi->flags, 2))
       {
       write_serial_string("cmdline = %s\n", (char *) mbi->cmdline);
         printf ("cmdline = %s\n", (char *) mbi->cmdline);
     }
       /* Are mods_* valid? */
       if (CHECK_FLAG (mbi->flags, 3))
         {
           multiboot_module_t *mod;
           int i;
            write_serial_string("mods_count = %d, mods_addr = 0x%x\n",
                   (int) mbi->mods_count, (int) mbi->mods_addr);
           printf ("mods_count = %d, mods_addr = 0x%x\n",
                   (int) mbi->mods_count, (int) mbi->mods_addr);
           for (i = 0, mod = (multiboot_module_t *) mbi->mods_addr;
                i < mbi->mods_count;
                i++, mod++)
             {
              write_serial_string(" mod_start = 0x%x, mod_end = 0x%x, cmdline = %s\n",
                     (unsigned) mod->mod_start,
                     (unsigned) mod->mod_end,
                     (char *) mod->cmdline);
             printf (" mod_start = 0x%x, mod_end = 0x%x, cmdline = %s\n",
                     (unsigned) mod->mod_start,
                     (unsigned) mod->mod_end,
                     (char *) mod->cmdline);
                     
                     }
         }
     
       /* Bits 4 and 5 are mutually exclusive! */
       if (CHECK_FLAG (mbi->flags, 4) && CHECK_FLAG (mbi->flags, 5))
         {
         write_serial_string("Both bits 4 and 5 are set.\n");
           printf ("Both bits 4 and 5 are set.\n");
           return 1;
         }
     
       /* Is the symbol table of a.out valid? */
       if (CHECK_FLAG (mbi->flags, 4))
         {
           multiboot_aout_symbol_table_t *multiboot_aout_sym = &(mbi->u.aout_sym);
           write_serial_string("multiboot_aout_symbol_table: tabsize = 0x%0x, "
                   "strsize = 0x%x, addr = 0x%x\n",
                   (unsigned) multiboot_aout_sym->tabsize,
                   (unsigned) multiboot_aout_sym->strsize,
                   (unsigned) multiboot_aout_sym->addr);
           printf ("multiboot_aout_symbol_table: tabsize = 0x%0x, "
                   "strsize = 0x%x, addr = 0x%x\n",
                   (unsigned) multiboot_aout_sym->tabsize,
                   (unsigned) multiboot_aout_sym->strsize,
                   (unsigned) multiboot_aout_sym->addr);
         }
     
       /* Is the section header table of ELF valid? */
       if (CHECK_FLAG (mbi->flags, 5))
         {
           multiboot_elf_section_header_table_t *multiboot_elf_sec = &(mbi->u.elf_sec);
           write_serial_string("multiboot_elf_sec: num = %u, size = 0x%x,"
                   " addr = 0x%x, shndx = 0x%x\n",
                   (unsigned) multiboot_elf_sec->num, (unsigned) multiboot_elf_sec->size,
                   (unsigned) multiboot_elf_sec->addr, (unsigned) multiboot_elf_sec->shndx);
           printf ("multiboot_elf_sec: num = %u, size = 0x%x,"
                   " addr = 0x%x, shndx = 0x%x\n",
                   (unsigned) multiboot_elf_sec->num, (unsigned) multiboot_elf_sec->size,
                   (unsigned) multiboot_elf_sec->addr, (unsigned) multiboot_elf_sec->shndx);
         }
     
       /* Are mmap_* valid? */
       if (CHECK_FLAG (mbi->flags, 6))
         {
           multiboot_memory_map_t *mmap;
           write_serial_string("mmap_addr = 0x%x, mmap_length = 0x%x\n",
                   (unsigned) mbi->mmap_addr, (unsigned) mbi->mmap_length);
           printf ("mmap_addr = 0x%x, mmap_length = 0x%x\n",
                   (unsigned) mbi->mmap_addr, (unsigned) mbi->mmap_length);
           for (mmap = (multiboot_memory_map_t *) mbi->mmap_addr;
                (unsigned long) mmap < mbi->mmap_addr + mbi->mmap_length;
                mmap = (multiboot_memory_map_t *) ((unsigned long) mmap
                                         + mmap->size + sizeof (mmap->size)))
           {
             write_serial_string(" size = 0x%x, base_addr = 0x%x%08x,"
                     " length = 0x%x%08x, type = 0x%x\n",
                     (unsigned) mmap->size,
                     (unsigned) (mmap->addr >> 32),
                     (unsigned) (mmap->addr & 0xffffffff),
                     (unsigned) (mmap->len >> 32),
                     (unsigned) (mmap->len & 0xffffffff),
                     (unsigned) mmap->type);
             printf (" size = 0x%x, base_addr = 0x%x%08x,"
                     " length = 0x%x%08x, type = 0x%x\n",
                     (unsigned) mmap->size,
                     (unsigned) (mmap->addr >> 32),
                     (unsigned) (mmap->addr & 0xffffffff),
                     (unsigned) (mmap->len >> 32),
                     (unsigned) (mmap->len & 0xffffffff),
                     (unsigned) mmap->type);
                     }
         }
         
          /* Draw diagonal blue line. */
       if (CHECK_FLAG (mbi->flags, 12))
         {
           multiboot_uint32_t color;
           unsigned i;
           void *fb = (void *) (unsigned long) mbi->framebuffer_addr;
           write_serial_string("mbi->framebuffer_addr: 0x%X\n", mbi->framebuffer_addr);
           write_serial_string("mbi->framebuffer_type: 0x%X\n", mbi->framebuffer_type);
           write_serial_string("mbi->framebuffer_pitch: 0x%X\n", mbi->framebuffer_pitch);
           write_serial_string("mbi->framebuffer_width: 0x%X\n", mbi->framebuffer_width);
           write_serial_string("mbi->framebuffer_height: 0x%X\n", mbi->framebuffer_height);
           write_serial_string("mbi->framebuffer_bpp: 0x%X bits per pixel\n", mbi->framebuffer_bpp);
           switch (mbi->framebuffer_type)
             {
             case MULTIBOOT_FRAMEBUFFER_TYPE_INDEXED:
               {
                 unsigned best_distance, distance;
                 struct multiboot_color *palette;
     
                 palette = (struct multiboot_color *) mbi->framebuffer_palette_addr;
     
                 color = 0;
                 best_distance = 4*256*256;
     
                 for (i = 0; i < mbi->framebuffer_palette_num_colors; i++)
                   {
                     distance = (0xff - palette[i].blue) * (0xff - palette[i].blue)
                       + palette[i].red * palette[i].red
                       + palette[i].green * palette[i].green;
                     if (distance < best_distance)
                       {
                         color = i;
                         best_distance = distance;
                       }
                   }
               }
               break;
     
             case MULTIBOOT_FRAMEBUFFER_TYPE_RGB:
               color = ((1 << mbi->framebuffer_blue_mask_size) - 1)
                 << mbi->framebuffer_blue_field_position;
               break;
     
             case MULTIBOOT_FRAMEBUFFER_TYPE_EGA_TEXT:
               color = '\\' | 0x0100;
               break;
     
             default:
               color = 0xffffffff;
               break;
             }
           for (i = 0; i < mbi->framebuffer_width
                  && i < mbi->framebuffer_height; i++)
             {
               switch (mbi->framebuffer_bpp)
                 {
                 case 8:
                   {
                     multiboot_uint8_t *pixel = fb + mbi->framebuffer_pitch * i + i;
                     *pixel = color;
                   }
                   break;
                 case 15:
                 case 16:
                   {
                     multiboot_uint16_t *pixel
                       = fb + mbi->framebuffer_pitch * i + 2 * i;
                     *pixel = color;
                   }
                   break;
                 case 24:
                   {
                     multiboot_uint32_t *pixel
                       = fb + mbi->framebuffer_pitch * i + 3 * i;
                     *pixel = (color & 0xffffff) | (*pixel & 0xff000000);
                   }
                   break;
     
                 case 32:
                   {
                     multiboot_uint32_t *pixel
                       = fb + mbi->framebuffer_pitch * i + 4 * i;
                     *pixel = color;
                   }
                   break;
                 }
             }
         }
  return 0;
}

void timer_test(void)
{

init_timer(50);
}
int compareArry(const void *a, const void *b) 
{    
      #ifdef QSORT_TEST
     write_serial_string("compareArry: %i\n", 1);
     #endif
    int * c = (int * ) a;
    int * d = (int * ) b;
    #ifdef QSORT_TEST
    write_serial_string("compareArry: %i, c: 0x%X, d: 0x%X\n  ", 2, c, d);
    write_serial_string("compareArry: %i, *c: 0x%X, *d: 0x%X\n  ", 2, *c, *d);
    #endif
    if (*c < *d)
    {
    return -1;
    }
    else if (*c > *d)
    {
    return 1;
    }
    else
    {
    return 0;
    }
}

void kernel_main(unsigned long magic, unsigned long addr) {//, unsigned long initial_stack) {
    //initial_esp = initial_stack;
    // Start multitasking.
    extern char __stext; 
    write_serial_string("__stext: 0x%X\n", &__stext);
    //initialise_tasking();
    
    #ifdef MULTIBOOT_TEST
     int mbtest_res = multiboot_test(magic, addr);
     if (mbtest_res != 0)
     {  return; }
     
    #endif
    write_serial_string("sizeof(wchar_t): %i\n", sizeof(wchar_t));
    int arry[10] = { 5, 3, 22, 44, 2, 1, 9, 8, 7, 6};
    qsort((void *)arry, 10, sizeof(int), compareArry);
    for (int i = 0; i < 10; i++)
    {
      write_serial_string("%i\n", arry[i]);
    }
    //initTasking();
    // Create a new process in a new address space which is a clone of this.
    //int ret = fork();
    //write_serial_string("fork() returned 0x%X, and getpid() returned 0x%X\n============================================================================\n", ret, getpid());

#ifdef PAGE_TEST
  asm volatile ("mov %cr0, %eax");
  init_stuff();
  #endif
  more_tests();
  #ifdef PAGE_TEST
  uint32_t *ptr = (uint32_t*)0xA0000000;
    uint32_t do_page_fault = *ptr;
    
    ptr = (uint32_t*)0xD0000000;
    do_page_fault = *ptr;
    printf("0x%X\n", do_page_fault);
    #endif
   //timer_test();
	printf("Hello, kernel World!\n");
	//do_tests();
   const char str[67] = "hi";
   printf("%d\n", sizeof(str));
   #ifdef HEAP_TEST
   uint32_t b = kmalloc(8);
   uint32_t c = kmalloc(8);
   write_serial_string("b: 0x%x\n", b);
   printf("b: 0x%x\n", b);
   write_serial_string("c: 0x%x\n", c);
   printf("c: 0x%x\n", c);
   kfree(c);
  kfree(b);
  uint32_t d = kmalloc(12);
  write_serial_string("d: 0x%x\n", d);
  printf("d: 0x%x\n", d);
  uint32_t * e = malloc(20);
  write_serial_string("e: 0x%x\n", &e[0]);
  printf("e: 0x%x\n", e);
  uint32_t * f = malloc(24);
  write_serial_string("f: 0x%x\n", f);
  printf("f: 0x%x\n", f);
  free(e);
  uint32_t * g = malloc(20);
  write_serial_string("g: 0x%x\n", g);
  printf("g: 0x%x\n", g);
  free(f);
  free(g);
  
  #endif
  char my_str[] = "hello";
  write_serial_string("[%s], contains, %i chars\n", my_str, strlen(my_str));
  
  //initialise_syscalls();
  printf("&switch_to_user_mode(): 0x%X\n&syscall_terminal_writestring(): 0x%X\n", &switch_to_user_mode,  &syscall_terminal_writestring);
  //switch_to_user_mode();
  hop_to_user_mode();
  syscall_terminal_writestring("Hello, user world!\n");
 //asm volatile ("int $0x3");
  //asm volatile ("int $0x4");
  
  //abort();
   //asm volatile ("int $0xe");
   //for(;;) {
    //asm("hlt");
 //}
}
