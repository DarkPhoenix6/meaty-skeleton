//
// isr.c -- High level interrupt service routines and interrupt request handlers.
//          Part of this code is modified from Bran's kernel development tutorials.
//          Rewritten for JamesM's kernel development tutorials.
//


#include <irqs/isr.h>
#include <irqs/inline_assembly.h>
#include <kernel/tty.h>
#include <kernel/com_one.h>
#include <stdio.h>
#include <stdint.h>
#undef PAGE_FAULT_DEBUG
isr_t interrupt_handlers[INT_HAND_SIZE];

void register_interrupt_handler(uint8_t n, isr_t handler)
{
    interrupt_handlers[n] = handler;
}

// This gets called from our ASM interrupt handler stub.
void isr_handler(registers_t regs)
{
    uint8_t int_no = regs.int_no & 0xFF;
    //printf("recieved interrupt: %d\n", regs.int_no);
    if (interrupt_handlers[int_no] != 0 )
    {
        isr_t handler = interrupt_handlers[int_no];
        #ifdef PAGE_FAULT_DEBUG
        if (int_no == 14)
          {
          write_serial_string("recieved interrupt: %d\n", regs.int_no);
          printf("recieved interrupt: %d\n", regs.int_no);
          }
        #endif
        handler(&regs);
    }
    else
    {
    write_serial_string("Unhandled interrupt: %u\n", int_no);
    printf("Unhandled interrupt: %u\n", int_no);
    for(;;);
    }
}

// This gets called from our ASM interrupt handler stub.
void irq_handler(registers_t regs)
{
    // Send an EOI (end of interrupt) signal to the PICs.
    // If this interrupt involved the slave.
    if (regs.int_no >= 40)
    {
        // Send reset signal to slave.
        outb(0xA0, 0x20);
    }
    // Send reset signal to master. (As well as slave, if necessary).
    outb(0x20, 0x20);

    if (interrupt_handlers[regs.int_no] != 0)
    {
        isr_t handler = interrupt_handlers[regs.int_no];
        handler(&regs);
    }

}

