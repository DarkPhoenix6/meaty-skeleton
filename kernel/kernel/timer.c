// timer.c -- Initialises the PIT, and handles clock updates.
//            Written for JamesM's kernel development tutorials.
#include <stdio.h>
#include <stdint.h>
#include <irqs/timer.h>
#include <kernel/task.h>
#include <kernel/tty.h>
#include <irqs/isr.h>
#include <irqs/inline_assembly.h>
#include <CMOS/cmos.h>
extern uint32_t Timer_val;
uint32_t tick = 0;
uint32_t volatile timer_ticks = 0;

static void timer_callback(registers_t* regs __attribute__((unused)))
{
    tick++;
    //yield();
    //SwapBuffers();  // no real need to do that but what-evs
    switch_task();
    //printf("Tick: %u\n", tick);

}

void init_timer(uint32_t frequency)
{
    //printf("0x%x\n", frequency);
   bool interrupts_en = are_interrupts_enabled();
   disable_interrupts();
    Timer_val = frequency; // in Hz
    // Firstly, register our timer callback.
    
    register_interrupt_handler(IRQ0, &timer_callback); // TODO: should this be &timer_callback ?

    // The value we send to the PIT is the value to divide it's input clock
    // (1193180 Hz) by, to get our required frequency. Important to note is
    // that the divisor must be small enough to fit into 16-bits.
    uint32_t divisor = 1193180 / frequency;

    // Send the command byte.
    outb(0x43, 0x36);

    // Divisor has to be sent byte-wise, so split here into upper/lower bytes.
    uint8_t l = (uint8_t)(divisor & 0xFF);
    uint8_t h = (uint8_t)( (divisor>>8) & 0xFF );

    // Send the frequency divisor.
    outb(0x40, l);
    outb(0x40, h);
    if (interrupts_en == true)
    {
      enable_interrupts();
    }
}

void init_timer_50ms() {
    init_timer((uint32_t) 50);
}

void init_timer_Timer_val() {
  init_timer(Timer_val);
}


/* This will continuously loop until the given time has
*  been reached */
void timer_wait(int ticks)
{
    unsigned long eticks;

    eticks = timer_ticks + ticks;
    while(timer_ticks < eticks);
}
