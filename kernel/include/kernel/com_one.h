#ifndef _KERNEL_COM_ONE_H
#define _KERNEL_COM_ONE_H

#include <stddef.h>
#include <irqs/inline_assembly.h>

 
void init_serial();

int serial_received();
 
char read_serial();

int is_transmit_empty();
 
void write_serial(char a);
int write_serial_string(const char* restrict format, ...);

#endif