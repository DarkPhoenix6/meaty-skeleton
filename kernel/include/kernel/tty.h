#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <stddef.h>

void SwapBuffers(); // Not Valid
void terminal_initialize(void);
void terminal_putchar(char c);
void terminal_write(const char* data, size_t size);
void terminal_writestring(const char* data);
void terminal_newline();
void monitor_clear();
// Output a null-terminated ASCII string to the monitor.
void monitor_write(char *c); //not valid
//void terminal_change_colour(enum vga_color fg, enum vga_color bg);
#endif
