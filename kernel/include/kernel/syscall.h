// syscall.h -- Defines the interface for and structures relating to the syscall dispatch system.
//              Written for JamesM's kernel development tutorials.

#ifndef SYSCALL_H
#define SYSCALL_H

#include <stdio.h>
#define	SYS_SBRK					0
#define	SYS_READ					1
#define	SYS_WRITE					2
#define SYS_OPEN					3
#define SYS_CLOSE					4
#define SYS_FORK					5
#define SYS_NANOSLEEP       		6
#define SYS_EXECVE					7
#define SYS_GETPID					8
#define SYS_EXIT					9
#define SYS_WAITPID					10
#define SYS_GETPPID					11
#define SYS_LSEEK					12
#define SYS_FSTAT					13
#define SYS_STAT					14
#define SYS_ISATTY					15
#define SYS_PIPE					16      
#define SYS_LSTAT					17
#define SYS_FCNTL					18
#define SYS_DUP2					19
#define SYS_SOCKET					20
#define SYS_CONNECT					21
#define SYS_SELECT					22
#define SYS_GETCWD					23
#define SYS_SETCWD					24
#define SYS_BIND					25
#define SYS_LISTEN					26
#define SYS_ACCEPT					27
#define SYS_GETDENTS				28  
#define SYS_MKDIR					29
#define SYS_CHMOD					30
#define SYS_IOCTL					31
#define SYS_RMDIR					32
#define SYS_MKNOD					33
#define SYS_FSYNC					34
#define SYS_FTRUNCATE				35
#define SYS_ACCESS					36
#define SYS_SENDTO					37
#define SYS_RECVFROM				38
#define SYS_GETSOCKNAME				39
#define SYS_GETPEERNAME				40
#define SYS_POLL					41
#define SYS_CLOCK_GETTIME			42
#define SYS_UNLINK      			43
#define SYS_LINK        			44
#define SYS_SYMLINK					45
#define SYS_READLINK				46
#define SYS_GETUID					47
#define SYS_SETUID					48
#define SYS_GETEUID					49
#define SYS_SETEUID					50
#define SYS_SETREUID				51
#define SYS_GETGID					52
#define SYS_SETGID					53
#define SYS_GETEGID					54
#define SYS_SETEGID					55
#define SYS_SETREGID				56
#define SYS_GETGROUPS				57
#define SYS_SETGROUPS				58
#define SYS_GETGRGID				59
#define SYS_UMASK					60
#define SYS_CHOWN					61
#define SYS_LCHOWN					62
#define SYS_FCHOWN					63
#define SYS_FCHMOD					64
#define SYS_TRUNCATE				65
#define SYS_SCHED_GETPARAM			66
#define SYS_SCHED_GETSCHEDULER		67
#define SYS_SCHED_GET_PRIORITY_MAX	68
#define SYS_SCHED_GET_PRIORITY_MIN	69
#define SYS_SCHED_SETPARAM			70
#define SYS_SCHED_SETSCHEDULER		71
#define SYS_SCHED_YIELD				72
#define SYS_MOUNT					73
#define SYS_UMOUNT					74
#define SYS_KILL					75
#define SYS_CLOCK_SETTIME			76
#define SYS_NET_CONFIGURE			77
#define SYS_UNAME					78
#define SYS_HOSTNAME				79
#define SYS_DOMAINNAME				80
#define SYS_CHTIMES					81
#define SYS_REBOOT					82
#define SYS_SYNC					83
#define SYS_STATFS					84
#define SYS_MMAP					85
#define SYS_MUNMAP					86

void initialise_syscalls();

#undef NEWLIB_SYSCALLS
#ifndef NEWLIB_SYSCALLS

#define DECL_SYSCALL0(fn) int syscall_##fn();
#define DECL_SYSCALL1(fn,p1) int syscall_##fn(p1);
#define DECL_SYSCALL2(fn,p1,p2) int syscall_##fn(p1,p2);
#define DECL_SYSCALL3(fn,p1,p2,p3) int syscall_##fn(p1,p2,p3);
#define DECL_SYSCALL4(fn,p1,p2,p3,p4) int syscall_##fn(p1,p2,p3,p4);
#define DECL_SYSCALL5(fn,p1,p2,p3,p4,p5) int syscall_##fn(p1,p2,p3,p4,p5);

#define DEFN_SYSCALL0(fn, num) \
int syscall_##fn() \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num)); \
  return a; \
}

#define DEFN_SYSCALL1(fn, num, P1) \
int syscall_##fn(P1 p1) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1)); \
  return a; \
}

#define DEFN_SYSCALL2(fn, num, P1, P2) \
int syscall_##fn(P1 p1, P2 p2) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2)); \
  return a; \
}

#define DEFN_SYSCALL3(fn, num, P1, P2, P3) \
int syscall_##fn(P1 p1, P2 p2, P3 p3) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2), "d"((int)p3)); \
  return a; \
}

#define DEFN_SYSCALL4(fn, num, P1, P2, P3, P4) \
int syscall_##fn(P1 p1, P2 p2, P3 p3, P4 p4) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2), "d" ((int)p3), "S" ((int)p4)); \
  return a; \
}

#define DEFN_SYSCALL5(fn, num) \
int syscall_##fn(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2), "d" ((int)p3), "S" ((int)p4), "D" ((int)p5)); \
  return a; \
}

DECL_SYSCALL1(monitor_write, const char*)
DECL_SYSCALL1(terminal_writestring, const char*)
DECL_SYSCALL1(kmalloc, uint32_t)
DECL_SYSCALL2(kmalloc_p, uint32_t, uint32_t *)

#else

#define DECL_SYSCALL0(fn) int ##fn();
#define DECL_SYSCALL1(fn,p1) int ##fn(p1);
#define DECL_SYSCALL2(fn,p1,p2) int ##fn(p1,p2);
#define DECL_SYSCALL3(fn,p1,p2,p3) int ##fn(p1,p2,p3);
#define DECL_SYSCALL4(fn,p1,p2,p3,p4) int ##fn(p1,p2,p3,p4);
#define DECL_SYSCALL5(fn,p1,p2,p3,p4,p5) int ##fn(p1,p2,p3,p4,p5);

#define DEFN_SYSCALL0(fn, num) \
int ##fn() \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num)); \
  return a; \
}

#define DEFN_SYSCALL1(fn, num, P1) \
int ##fn(P1 p1) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1)); \
  return a; \
}

#define DEFN_SYSCALL2(fn, num, P1, P2) \
int ##fn(P1 p1, P2 p2) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2)); \
  return a; \
}

#define DEFN_SYSCALL3(fn, num, P1, P2, P3) \
int ##fn(P1 p1, P2 p2, P3 p3) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2), "d"((int)p3)); \
  return a; \
}

#define DEFN_SYSCALL4(fn, num, P1, P2, P3, P4) \
int ##fn(P1 p1, P2 p2, P3 p3, P4 p4) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2), "d" ((int)p3), "S" ((int)p4)); \
  return a; \
}

#define DEFN_SYSCALL5(fn, num) \
int ##fn(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5) \
{ \
  int a; \
  asm volatile("int $0x80" : "=a" (a) : "0" (num), "b" ((int)p1), "c" ((int)p2), "d" ((int)p3), "S" ((int)p4), "D" ((int)p5)); \
  return a; \
}
DECL_SYSCALL1(syscall_monitor_write, const char*)
DECL_SYSCALL1(syscall_terminal_writestring, const char*)
DECL_SYSCALL1(syscall_kmalloc, uint32_t)
DECL_SYSCALL2(syscall_kmalloc_p, uint32_t, uint32_t *)

#endif



#endif
