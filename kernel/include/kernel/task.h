#ifdef ASM_FILE


#define KERNEL_STACK_SIZE 2048       // Use a 2kb kernel stack.
#define TASK_POINTER_KERNEL_STACK_OFFSET 0x14
#else
// 
// task.h - Defines the structures and prototypes needed to multitask.
//          Written for JamesM's kernel development tutorials.
//
#ifndef TASK_H
#define TASK_H

#include <stdint.h>
#include "paging.h"

#ifdef KERNEL_STACK_SIZE
#undef KERNEL_STACK_SIZE
#endif
#define KERNEL_STACK_SIZE 2048       // Use a 2kb kernel stack.

extern void initTasking();
 
typedef struct {
    uint32_t eax, ebx, ecx, edx, esi, edi, esp, ebp, eip, eflags, cr3;
} Registers;
 
typedef struct Task {
    Registers regs;
    struct Task *next;
} Task;
 
extern void initTasking();
extern void createTask(Task*, void(*)(), uint32_t, uint32_t*);
 
extern void yield(); // Switch task frontend
extern void switchTask(Registers *old, Registers *new_reg); // The function which actually switches

// This structure defines a 'task' - a process.
typedef struct task
{
    int id;                // Process ID.                      // + 0x00
    uint32_t esp, ebp;       // Stack and base pointers.       // + 0x04
    uint32_t eip;            // Instruction pointer.           // + 0x08
    page_directory_t *page_directory; // Page directory.       // + 0x10
    uint32_t kernel_stack;   // Kernel stack location.         // + 0x14
    struct task *next;     // The next task in a linked list.  // + 0x18
} task_t;

// Initialises the tasking system.
void initialise_tasking();

void switch_to_user_mode();  // GCC doesn't play nice with the stack count in this function.... so it may try to return to an improper location.   USE hop_to_user_mode INSTEAD

// in process.S
extern void hop_to_user_mode();

// Called by the timer hook, this changes the running process.
void task_switch(); // Not Valid
void switch_task();

// Forks the current process, spawning a new one with a different
// memory space.
int fork();

// Causes the current process' stack to be forcibly moved to a new location.
void move_stack(void *new_stack_start, uint32_t size);

// Returns the pid of the current process.
int getpid();

#endif
#endif

