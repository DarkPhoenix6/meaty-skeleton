#ifndef _KERNEL_SYSTEM_H
#define _KERNEL_SYSTEM_H
#include <stdint.h>
#include <stddef.h>
#define PANIC(msg) panic(msg, __FILE__, __LINE__);
#define ASSERT(b) ((b) ? (void)0 : panic_assert(__FILE__, __LINE__, #b))

extern void panic(const char *message, const char *file, uint32_t line);
extern void panic_assert(const char *file, uint32_t line, const char *desc);
void kmemcpy(uint8_t *dest, const uint8_t *src, uint32_t len);
void kmemsetw(uint16_t * ptr, uint16_t wc, size_t num);
void *sbrk(size_t size);
int brk(const void *addr );

#endif
