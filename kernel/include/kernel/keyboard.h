#ifndef _KERNEL_KEYBOARD_H
#define _KERNEL_KEYBOARD_H 1
#include <irqs/isr.h>
void init_keyboard_buffer();


/* Handles the keyboard interrupt */
void keyboard_handler(registers_t* regs); // not yet working

#endif /* _KERNEL_KEYBOARD_H */
