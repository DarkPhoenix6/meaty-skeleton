#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H 1
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <kernel/tty.h>
#include <multiboot/multiboot.h>
#include <filesystem/fs.h>
#include <filesystem/initrd.h>
#include <kernel/com_one.h>
#include <kernel/system.h>
#include <kernel/syscall.h>
#include <irqs/irqs.h>
#include <irqs/myCPUID.h>
#include <irqs/interrupt_tables.h>
#include <irqs/inline_assembly.h>
#include <irqs/timer.h>
#include <CMOS/cmos.h>
#include <CMOS/pit.h>
#include <kernel/paging.h>
#include <kernel/kheap.h>
#include <kernel/task.h>
#include <kernel/keyboard.h>
//struct IDTDescr IDT;

		 


//char 1
// short 2
// int 4
// long 4
// long long 8
// float 4
// double 8
// long double 10

    /* Macros. */
     
     /* Check if the bit BIT in FLAGS is set. */
     #define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))
//#define printf(a, ...) _Generic(a, char: printf_c, char*: printf, bool: printf_b, uint8_t: print_u_int8_t, uint16_t: print_u_int16_t, unsigned int: print_u_int, unsigned long long: print_u_long_long)(a)
#define MULTIBOOT_FLAG_MEM     0x001
#define MULTIBOOT_FLAG_DEVICE  0x002
#define MULTIBOOT_FLAG_CMDLINE 0x004
#define MULTIBOOT_FLAG_MODS    0x008
#define MULTIBOOT_FLAG_AOUT    0x010
#define MULTIBOOT_FLAG_ELF     0x020
#define MULTIBOOT_FLAG_MMAP    0x040
#define MULTIBOOT_FLAG_CONFIG  0x080
#define MULTIBOOT_FLAG_LOADER  0x100
#define MULTIBOOT_FLAG_APM     0x200
#define MULTIBOOT_FLAG_VBE     0x400
#undef PAGE_TEST
#define HEAP_TEST 1
#define MULTIBOOT_TEST 1


void more_tests();
void _init_interupts();
void init_stuff();
void do_tests();
int multiboot_test(unsigned long magic, unsigned long addr);
void timer_test(void);

#endif
