#ifndef ARCH_I386MACH_MACROS_H
#define ARCH_I386MACH_MACROS_H 1
#define ASM_FILE        1
#include "i386mach.h"
/* C symbol format. HAVE_ASM_USCORE is defined by configure. */
#ifdef HAVE_ASM_USCORE
# define EXT_C(sym)                     _ ## sym
#else
# define EXT_C(sym)                     sym
#endif

#endif 
