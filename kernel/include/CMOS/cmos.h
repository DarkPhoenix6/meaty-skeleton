#ifndef _CMOS_CMOS_H
#define _CMOS_CMOS_H 1
#include <irqs/inline_assembly.h>
#include <irqs/irqs.h>
#include <stdio.h>
#define RTC_DEFAULT_FREQ 32768

#ifdef __cplusplus
extern "C" {
#endif
#include <CMOS/pit.h>

#undef CMOS_READ_WRITE_TEST
#ifdef CMOS_READ_WRITE_TEST
void ReadFromCMOS (unsigned char array []);

void WriteTOCMOS(unsigned char array[]);

#endif /* CMOS_READ_WRITE_TEST */
int get_update_in_progress_flag();
unsigned char get_RTC_register(int reg);
void read_rtc(unsigned char *second, unsigned char *minute, unsigned char *hour, unsigned char *day, unsigned char *month, unsigned int *year, int *century_register);
void print_date();
#ifdef __cplusplus
}
#endif
#endif /* _CMOS_CMOS_H */
