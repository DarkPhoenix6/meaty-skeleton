#ifndef _CMOS_PIT_H
#define _CMOS_PIT_H 1

#include <stdint.h>
#include <CMOS/cmos.h>
#ifdef __cplusplus
extern "C" {
#endif
uint8_t read_register_c();
void turn_on_IRQ8();
void change_interrupt_rate(int rate);
#ifdef __cplusplus
}
#endif


#endif
