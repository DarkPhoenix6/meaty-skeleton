#ifndef _IRQS_MYCPUID_H
#define _IRQS_MYCPUID_H
#include <cpuid.h>
#include <stdbool.h>


#define CPUID_FLAG_APIC (1 << 9)
#define IA32_APIC_BASE_MSR 0x1B
#define IA32_APIC_BASE_MSR_BSP 0x100 // Processor is a BSP
#define IA32_APIC_BASE_MSR_ENABLE 0x800
 
/** returns a 'true' value if the CPU supports APIC
 *  and if the local APIC hasn't been disabled in MSRs
 *  note that this requires CPUID to be supported.
 */
 bool check_apic();

#endif