#ifndef _IRQS_IRQS_H
#define _IRQS_IRQS_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <irqs/inline_assembly.h>
#include <irqs/interrupt_tables.h>
#include <CMOS/pit.h>
#define _IRQS_IRQS_H_PIC_REMAP 1
//static inline void enable_interrupts();
//static inline void disable_interrupts();
void PIC_sendEOI(unsigned char irq);
void PIC_remap(int offset1, int offset2);
void IRQ_set_mask(unsigned char IRQline);
void IRQ_clear_mask(unsigned char IRQline);

static uint16_t __pic_get_irq_reg(int ocw3);
uint16_t pic_get_irr(void);
uint16_t pic_get_isr(void);
#ifndef _IRQS_INLINE_ASSEMBLY_H
static inline void NMI_enable(void);
static inline void NMI_disable(void);

static inline void NMI_and_interrupts_enable(void);

static inline void NMI_and_interrupts_disable(void);



// Non-Maskable interrupts
static inline void NMI_enable(void)
{
   outb(0x70, inb(0x70)&0x7F);
}

static inline void NMI_disable(void)
{
   outb(0x70, inb(0x70)|0x80);
}


static inline void NMI_and_interrupts_enable(void)
{
   outb(0x70, inb(0x70)&0x7F);
   asm volatile ("sti"); /* Enable interrupts*/
}

static inline void NMI_and_interrupts_disable(void)
{
    asm volatile ("cli"); /* Disable interrupts*/
   outb(0x70, inb(0x70)|0x80);
}


//static inline void NMI_and_interrupts_enable(void)
//{
//   outb(0x70, inb(0x70)&0x7F);
//   asm volatile ("sti"); /* Enable interrupts*/
//}

//static inline void NMI_and_interrupts_disable(void)
//{
//    asm volatile ("cli"); /* Disable interrupts*/
//   outb(0x70, inb(0x70)|0x80);
//}
#endif





//#include "interrupt_tables.h"



#endif
