#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <kernel/kheap.h>
#include <kernel/tty.h>

#include "vga.h"
#include <irqs/inline_assembly.h>
#include <common/common.h>

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
static uint16_t* const VGA_MEMORY = (uint16_t*) 0xB8000;
static uint32_t back_buffer_addr;
static uint16_t* back_buffer;
static size_t terminal_row;
static size_t terminal_column;
static uint8_t terminal_color;
static uint16_t* terminal_buffer;
static uint16_t cursor_x; // column
static uint16_t cursor_y; // row
static const size_t BITS_PER_PIXEL = 16;
static const size_t PITCH = 160;
static const size_t VIDEO_MEMORY_SIZE = 4000; //(size_t) (sizeof(uint16_t) * VGA_WIDTH * VGA_HEIGHT); // VGA_HEIGHT * PITCH
bool buff_change = false;
/*
 * Swaps the back and front buffer.
 * Most commonly done by simply copying the back buffer to the front buffer.
 */
void SwapBuffers()
{
  if (buff_change)
  {
	/* Copy the contents of the back buffer to the front buffer. */
	//memcpy(terminal_buffer, back_buffer, VIDEO_MEMORY_SIZE);
  buff_change = false;
  }
 
}
// Updates the hardware cursor.
static void move_cursor()
{
   // The screen is 80 characters wide...
   unsigned short cursorLocation = cursor_y * VGA_WIDTH + cursor_x;
   outb(0x3D4, 14);                  // Tell the VGA board we are setting the high cursor byte.
   outb(0x3D5, cursorLocation >> 8); // Send the high cursor byte.
   outb(0x3D4, 15);                  // Tell the VGA board we are setting the low cursor byte.
   outb(0x3D5, cursorLocation);      // Send the low cursor byte.
}

// Scrolls the text on the screen up by one line.
static void scroll()
{

   // Get a space character with the default colour attributes.
   unsigned char attributeByte = (0 /*black*/ << 4) | (15 /*white*/ & 0x0F);
   unsigned short blank = 0x20 /* space */ | (attributeByte << 8);

   // Row 25 is the end, this means we need to scroll up
   if(cursor_y >= 25)
   {
       // Move the current text chunk that makes up the screen
       // back in the buffer by a line
       int i;
       for (i = 0*80; i < 24*80; i++)
       {
           //back_buffer[i] = terminal_buffer[i+80];
           terminal_buffer[i] = terminal_buffer[i+80];
       }

       // The last line should now be blank. Do this by writing
       // 80 spaces to it.
       for (i = 24*80; i < 25*80; i++)
       {
           //back_buffer[i] = blank;
           terminal_buffer[i] = blank;
       }
       // The cursor should now be on the last line.
       cursor_y = VGA_HEIGHT - 1;
   }
   terminal_row = cursor_y;
   //buff_change = true;
}

void terminal_initialize(void) {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
	terminal_buffer = VGA_MEMORY;
   //back_buffer_addr = kmalloc(VIDEO_MEMORY_SIZE);
   //back_buffer = back_buffer_addr;
  // *back_buffer = (uint16_t*) kmalloc_a((size_t) (sizeof(uint16_t) * VGA_WIDTH * VGA_HEIGHT));
  //memset(back_buffer, 0, VIDEO_MEMORY_SIZE);
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(' ', terminal_color);
      //back_buffer[index] = terminal_buffer[index];
		}
	}
  cursor_x = 0;
  cursor_y = 0;
  //SwapBuffers();
  move_cursor();
}

void terminal_newline()
{
  ++terminal_row;
	//if (++terminal_row == VGA_HEIGHT)
			//{terminal_row = 0;}
	terminal_column = 0;
   cursor_x = terminal_column;
   cursor_y = terminal_row;
  
}

void terminal_setcolor(uint8_t color) {
	terminal_color = color;
}

void terminal_change_colour(enum vga_color fg, enum vga_color bg)
{
  terminal_setcolor(vga_entry_color(fg, bg));
}

void terminal_putentryat(unsigned char c, uint8_t color, size_t x, size_t y) {
	const size_t index = y * VGA_WIDTH + x;
	//back_buffer[index] = vga_entry(c, color);
 terminal_buffer[index] = vga_entry(c, color);
 
}

void terminal_putchar(char c) {
	unsigned char uc = c;
	terminal_putentryat(uc, terminal_color, terminal_column, terminal_row);
	if (++terminal_column == VGA_WIDTH) {
		terminal_column = 0;
		++terminal_row;
			
	}
 cursor_x = terminal_column;
    cursor_y = terminal_row;
}

void terminal_write(const char* data, size_t size) {
	for (size_t i = 0; i < size; i++)
   {
		if (data[i] != '\n' && data[i] != '\r' && data[i] >= ' ')
   {
			terminal_putchar(data[i]);
   }else if (data[i] == 0x09)
   {
   // tab
      cursor_x = (cursor_x+8) & ~(8-1);
      terminal_column = cursor_x;
      if (terminal_column >= VGA_WIDTH)
        {
          terminal_column = 0;
          terminal_row++;
        }
		}else if(data[i] == 0x08 && cursor_x)
		{
      cursor_x--;
      terminal_column = cursor_x;
    } else if(data[i] == '\n')
		{
      terminal_newline();
    } else if(data[i] == '\r')
		{
      terminal_column = 0;
      cursor_x = 0;
    }
    cursor_x = terminal_column;
    cursor_y = terminal_row;
    // Scroll the screen if needed.
   scroll();
   // Move the hardware cursor.
   move_cursor();
   }
   //buff_change = true;
  //SwapBuffers();
}

void terminal_writestring(const char* data) {
	terminal_write(data, strlen(data));
}



// Writes a single character out to the screen.
void monitor_put(char c)
{
   // The background colour is black (0), the foreground is white (15).
   unsigned char backColour = 0;
   unsigned char foreColour = 15;

   // The attribute byte is made up of two nibbles - the lower being the
   // foreground colour, and the upper the background colour.
   unsigned char  attributeByte = (backColour << 4) | (foreColour & 0x0F);
   // The attribute byte is the top 8 bits of the word we have to send to the
   // VGA board.
   unsigned short attribute = attributeByte << 8;
   unsigned short *location;

   // Handle a backspace, by moving the cursor back one space
   if (c == 0x08 && cursor_x)
   {
       cursor_x--;
   }

   // Handle a tab by increasing the cursor's X, but only to a point
   // where it is divisible by 8.
   else if (c == 0x09)
   {
       cursor_x = (cursor_x+8) & ~(8-1);
   }

   // Handle carriage return
   else if (c == '\r')
   {
       cursor_x = 0;
   }

   // Handle newline by moving cursor back to left and increasing the row
   else if (c == '\n')
   {
       cursor_x = 0;
       cursor_y++;
   }
   // Handle any other printable character.
   else if(c >= ' ')
   {
       location = terminal_buffer + (cursor_y*80 + cursor_x);
       *location = c | attribute;
       cursor_x++;
   }

   // Check if we need to insert a new line because we have reached the end
   // of the screen.
   if (cursor_x >= 80)
   {
       cursor_x = 0;
       cursor_y ++;
   }

   // Scroll the screen if needed.
   scroll();
   // Move the hardware cursor.
   move_cursor();
}
// Outputs a null-terminated ASCII string to the monitor.
void monitor_write(char *c)
{
    int i = 0;
    while (c[i])
    {
        monitor_put(c[i++]);
    }
}
// Clears the screen, by copying lots of spaces to the framebuffer.
void monitor_clear()
{
   // Make an attribute byte for the default colours
   unsigned char attributeByte = (0 /*black*/ << 4) | (15 /*white*/ & 0x0F);
   unsigned short blank = 0x20 /* space */ | (attributeByte << 8);

   int i;
   for (i = 0; i < VGA_WIDTH*VGA_HEIGHT; i++)
   {
       //back_buffer[i] = blank;
       terminal_buffer[i] = blank;
   }

   // Move the hardware cursor back to the start.
   cursor_x = 0;
   cursor_y = 0;
   terminal_column = cursor_x;
   terminal_row = cursor_y;
   move_cursor();
   
}
