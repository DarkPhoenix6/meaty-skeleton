#!/bin/sh
set -e
. ./clean.sh
. ./iso.sh

qemu-system-$(./target-triplet-to-arch.sh $HOST) -serial file:./serial.log -cdrom darkphoenixos.iso